import time
from random import random

import networkx as nx
import numpy as np
from numpy.polynomial.polynomial import polyval
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
from sympy import symbols, Matrix, cos, sin, MatrixSymbol, eye, zeros, lambdify

G = nx.Graph()
# G.add_nodes_from([0, 1,2,3,4,5,6,7])
# G.add_edges_from([(0, 1),(0,3),(0,4),(1,2),(1,5),(2,3),(2,6),(2,4),(3,7),(4,5),(5,6),(6,7),(4,7)])
Agents = [0, 1, 2, 3]
Nb_agents = len(Agents)
G.add_nodes_from(Agents)  # ,6,7,8,9])
Edges = [(0, 1), (0, 3), (1, 3), (1, 2), (2, 3)]
Nb_edges = len(Edges)
G.add_edges_from(Edges)
deg_poly = 5
Nb_param = 2
""" 
theta_i,r,b,xi,yi = symbols("theta_i r b x_i y_i")
wr_i,wl_i = symbols("omega_li omega_ri")

P = Matrix([[r],[b]])
X = Matrix([[xi],[yi],[theta_i]])
B = Matrix([[cos(theta_i), 0],[sin(theta_i), 0],[0,1]])
R = Matrix([[r/2,r/2],[r/b,-r/b]])
U = Matrix([[wr_i],[wl_i]])
f = B*R*U
print(latex(X,mode="equation"))
print(latex(B,mode="equation"))
print(latex(R,mode="equation"))
print(latex(U,mode="equation"))
print(latex(f,mode="equation"))
"""

### vecteur parametres reel ###
p_s = Matrix(MatrixSymbol('p', Nb_param, 1))  # r,b
### vecteur parametres controleur ###
pc_s = Matrix(MatrixSymbol('pc', Nb_param, 1))  # r,b
### vecteur des coef de réglages ###
kc_s = Matrix(MatrixSymbol('kc', 2, 1))
### definition du vecteur etat ####
Q_s = Matrix(MatrixSymbol('Q', Nb_agents, 3))
Q_des = Matrix(MatrixSymbol('V_des', Nb_agents, 2))
### definition du vecteur d'entrées ###
U = Matrix(MatrixSymbol('U', Nb_agents, 2))
### definition de la matrice de sensibilité ###
Pi = Matrix(MatrixSymbol('Pi', Nb_agents * 3, Nb_param))
### definition de R,Rc  ####
Rc = Matrix([[pc_s[0] / 2, pc_s[0] / 2], [pc_s[0] / pc_s[1], -pc_s[0] / pc_s[1]]])
R = Matrix([[p_s[0] / 2, p_s[0] / 2], [p_s[0] / p_s[1], -p_s[0] / p_s[1]]])
### dictionnaire symbolique pour le vecteur edges ####
### a améilorier pour etre adaptable a tous les codes facilement ###
dict_s = {}
X01, Y01, X03, Y03, X13, Y13, X12, Y12, X23, Y23 = symbols("X01 Y01 X03 Y03 X13 Y13 X12 Y12 X23 Y32")
dict_s[str([0, 1])] = Matrix([X01, Y01])
dict_s[str([0, 3])] = Matrix([X03, Y03])
dict_s[str([1, 2])] = Matrix([X12, Y12])
dict_s[str([1, 3])] = Matrix([X13, Y13])
dict_s[str([2, 3])] = Matrix([X23, Y23])

Vd = Matrix([X01, Y01, X03, Y03, X13, Y13, X12, Y12, X23, Y23])


def getpoly2dict_symbolique(dict, a, v):
    try:
        res = dict[str([a, v])]
    except:
        res = -dict[str([v, a])]
    return res


# print(getpoly2dict_symbolique(dict_s, 0, 1))

# print(getpoly2dict_symbolique(dict_s, 1, 0))

### defintion de h ####
h = zeros(Nb_agents, 2)
for a in Agents:
    Voisins = [l for l in nx.neighbors(G, a)]
    for v in Voisins:
        eij_d = getpoly2dict_symbolique(dict_s, a, v)
        E = eij_d
        gijd = E / E.norm()
        # print(gijd)
        Pgijd = (eye(2) - gijd * gijd.T)
        Sc = Rc.inv() * Matrix([[cos(Q_s[a, 2]), sin(Q_s[a, 2])], [-sin(Q_s[a, 2]), cos(Q_s[a, 2])]])
        E_new = Pgijd.dot((Q_s[v, 0:2] - Q_s[a, 0:2]).T)
        h[a, :] = h[a, :] + Matrix(Sc.dot(E_new)).T

### definition de f ####
f = zeros(Nb_agents, 3)
for a in Agents:
    B = Matrix([[cos(Q_s[a, 2]), 0], [sin(Q_s[a, 2]), 0], [0, 1]])
    S = B * R
    f[a, :] = (Matrix(S.dot(U[a, :].T))).T
# f = Matrix(f)
### calcul des jacobiennes ####


Q_v = Q_s.reshape(Nb_agents * 3, 1)
U_v = U.reshape(Nb_agents * 2, 1)
f_v = f.reshape(Nb_agents * 3, 1)

df_dq = f_v.jacobian(Q_v)
df_dp = f_v.jacobian(p_s)
df_du = f_v.jacobian(U_v)

Q_des_v = Q_des.reshape(Nb_agents * 2, 1)
h_v = h.reshape(Nb_agents * 2, 1)

dh_dzdes = h_v.jacobian(Vd)
dZes_dp = Vd.jacobian(p_s)
dh_dq = h_v.jacobian(Q_v)
dh_dpc = h_v.jacobian(pc_s)
dpc_dp = pc_s.jacobian(p_s)
dh_dkc = h_v.jacobian(kc_s)
dkc_dp = kc_s.jacobian(p_s)

### définition de la sensibilité des entrées ####
Theta = dh_dzdes * dZes_dp + dh_dq * Pi + dh_dpc * dpc_dp + dh_dkc * dkc_dp

Pip = df_dq * Pi + df_du * Theta + df_dp

pip_numerique = lambdify([Q_s, Vd, p_s, pc_s, kc_s, Pi, U], Pip, 'numpy')
Qp = lambdify([Q_s, U, p_s], f, 'numpy')
entree = lambdify([Q_s, Vd, pc_s, kc_s], h, "numpy")
theta_numerique = lambdify([Q_s, Vd, p_s, pc_s, kc_s, Pi, U], Theta, 'numpy')


def vect2dict(V):
    B = {}
    i = 0
    for l in Edges:
        B[str([l[0], l[1]])] = [V[i], V[i + 1]]
        i = i + 2
    # print(B)
    return B


def getpoly2dict(dict, a, v):
    try:
        res = dict[str([a, v])]
    except:
        x, y = dict[str([v, a])]
        x = [-e for e in x]
        y = [-e for e in y]
        res = [x, y]
    return res


def dict2mat(dict):
    a = []
    for k in dict.keys():
        # print("clé : ", k, " valeurs : ", dict[k])
        a.append(dict[k][0])
        a.append(dict[k][1])
    # print(a)
    return np.array(a)


def dict2vect(dict):
    M = dict2mat(dict)
    C = M.reshape((1, len(Edges) * deg_poly * 2))
    C = C[0]
    return C


def createrandom_dict():
    dict = {}
    for i in range(len(Edges)):
        cle = str([Edges[i][0], Edges[i][1]])
        dict[cle] = [[random() for k in range(deg_poly)], [random() for k in range(deg_poly)]]
    return dict


def eval_poly_agents_voisins(a, v, dict, t):
    P = getpoly2dict(dict, a, v)
    # print(P)
    X = polyval(t, P[0])
    Y = polyval(t, P[1])
    return [X, Y]


def eval_poly(dict, t):
    res = []
    for e in Edges:
        Z = eval_poly_agents_voisins(e[0], e[1], dict, t)
        # print(e)
        res.append(Z)
    return res


Q0 = np.array(
    [[1 / np.sqrt(2), 1 / np.sqrt(2), 0], [-1 / np.sqrt(2), 1 / np.sqrt(2), 0], [-1 / np.sqrt(2), -1 / np.sqrt(2), 0],
     [1 / np.sqrt(2), -1 / np.sqrt(2), 0]])

Qfinal = Pd = np.array([[1, 0, 0], [0, 1, 0], [-1, 0, 0], [0, -1, 0]])
Tfinal = 10
Nb_points = 100
T = np.linspace(0, Tfinal, Nb_points)


def vecteur_final(Qfinal, Tfinal):
    V = []
    for e in Edges:
        xy = Qfinal[e[0], :2] - Qfinal[e[1], :2]
        V.append(xy)
    return V


V_f = np.array(vecteur_final(Qfinal, 0))
r = 0.1
b = 0.2
pc = np.array([[r], [b]])
u = entree(Q0.reshape((1, Nb_agents * 3))[0], V_f.reshape((1, Nb_edges * 2))[0], pc, [0, 0])
U = u.reshape((Nb_agents * 2, 1))

Pp = Qp(Q0.reshape((1, Nb_agents * 3))[0], U, pc)


# print(Pp)


def integrante(t, Q_bar):
    Q_t = Q_bar[0:Nb_agents * 3].reshape((1, Nb_agents * 3))[0]
    u = entree(Q_t, V_f.reshape((1, Nb_edges * 2))[0], pc, [0, 0])
    U = u.reshape((Nb_agents * 2, 1))
    Xp = Qp(Q_t, U, pc)
    # print(Xp)
    Xp = Xp.reshape((1, Nb_agents * 3))[0]
    Pi = Q_bar[Nb_agents * 3:]
    pi_new = pip_numerique(Q_t, V_f.reshape((1, Nb_edges * 2))[0], pc, pc, [0, 0], Pi, U)
    pi_new = pi_new.reshape((1, 3 * Nb_agents * Nb_param))[0]
    # print(pi_new)
    Q_bar_new = np.hstack((Xp, pi_new))
    return Q_bar_new


D = createrandom_dict()


def integrante_opti(t, Q_bar, D):
    Q_t = Q_bar[0:Nb_agents * 3].reshape((1, Nb_agents * 3))[0]
    V_t = eval_poly(D, t)
    # print(V_t)
    V_t = np.array(V_t)
    V_t_v = V_t.reshape((1, 2 * Nb_edges))[0]
    u = entree(Q_t, V_t_v, pc, [0, 0])
    U = u.reshape((Nb_agents * 2, 1))
    Xp = Qp(Q_t, U, pc)
    # print(Xp)
    Xp = Xp.reshape((1, Nb_agents * 3))[0]
    Pi = Q_bar[Nb_agents * 3:]
    pi_new = pip_numerique(Q_t, V_t_v, pc, pc, [0, 0], Pi, U)
    pi_new = pi_new.reshape((1, 3 * Nb_agents * Nb_param))[0]
    # print(pi_new)
    Q_bar_new = np.hstack((Xp, pi_new))
    return Q_bar_new


Pi0 = np.zeros((1, 3 * Nb_agents * Nb_param))
Q_bar_0 = np.hstack((Q0.reshape((1, Nb_agents * 3))[0], Pi0[0]))


# print(integrante_opti(0, Q_bar_0,D))
# start_time = time.time()
# Y = solve_ivp(integrante_opti, [0, Tfinal], Q_bar_0, t_eval=T)
# print("temps d'éxecution : %s " % (time.time() - start_time))


def extraction_solution(Y):
    temps = Y.t
    Q = Y.y[0:3 * Nb_agents]
    # print("Q = ",Q)
    Pi = Y.y[3 * Nb_agents:]
    # print("Pi = ",Pi)
    return temps, Q, Pi


def extraction_matrice_norme_tfinal(Pi):
    pic = Pi
    (a, b) = pic.shape
    nb_param = int(a / (Nb_agents * 2))
    pic = pic.reshape((Nb_agents * 2, nb_param, b))
    Mt = []
    Norme = []
    for k in range(b):
        M = np.zeros((Nb_agents * 2, nb_param))
        for i in range(Nb_agents * 2):
            for j in range(nb_param):
                M[i, j] = pic[i, j][k]
        Mt.append(M)
        Norme.append(np.linalg.norm(M))
    return Norme[-1]


# temps,Q,Pi = extraction_solution(Y)
# print(extraction_matrice_norme_tfinal(Pi))

def traj_fct_vect(V):
    dict = vect2dict(V)

    def fct_integrante(t, Q):
        return integrante_opti(t, Q, dict)

    Y = solve_ivp(fct_integrante, [0, Tfinal], Q_bar_0, t_eval=T)
    return Y


def objectif(V):
    Y = traj_fct_vect(V)
    temps, Q, Pi = extraction_solution(Y)
    norme = extraction_matrice_norme_tfinal(Pi)
    Qf = np.zeros((1, 2 * Nb_agents))
    for j in range(2 * Nb_agents):
        Qf[:, j] = Y.y[j][-1]
    Qf = Qf.reshape((Nb_agents, 2))
    Df = Qf - Qfinal[:, :2]
    N = 0
    for i in range(Nb_agents):
        # print(Df[i, :])
        N = N + np.linalg.norm(Df[i, :])
    return norme + N


def cout_final(a):
    dict = vect2dict(a)
    Vtf = eval_poly(dict, Tfinal)
    Df = Vtf - V_f
    N = 0
    for i in range(Nb_edges):
        # print(Df[i, :])
        N = N + np.linalg.norm(Df[i, :])
    return N


N_iter = 100
contrainte_final = {'type': 'eq', 'fun': cout_final}
opts = {'disp': True, 'maxiter': N_iter}
a = createrandom_dict()
a = dict2vect(a)
print("début opti")
start_time = time.time()
a_opti = minimize(objectif, a, method="SLSQP", constraints=contrainte_final, options=opts)
print("temps d'éxecution : %s " % (time.time() - start_time))
print("La solution opti trouvé est : ", a_opti.x)
import pylab as plt
from matplotlib import animation

P_opti = a_opti.x
dict_opti = vect2dict(P_opti)
print(dict_opti)

Y = traj_fct_vect(P_opti)
dimension_espace = 3
Nb_Agents = Nb_agents
fig = plt.figure()
lines = [plt.plot([], [])[0] for _ in range(Nb_Agents + len([e for e in nx.edges(G)]))]
legend = plt.legend()


def init():
    P_init = Q0.reshape((1, dimension_espace * Nb_Agents))
    # print(P_init)
    P_desire = Qfinal.reshape((1, dimension_espace * Nb_Agents))
    # print(P_desire)
    plt.plot(Q0.T[0], Q0.T[1], '>', label="Position initial")
    plt.plot(Qfinal.T[0], Qfinal.T[1], "<", label="Position désiré")
    # fermeture_initial = np.vstack((P_init, P_init[0])).T
    # fermeture_desire = np.vstack((P_desire, P_desire[0])).T
    # plt.plot(fermeture_initial[0], fermeture_initial[1])
    # plt.plot(fermeture_desire[0], fermeture_desire[1])
    m = True
    for e in nx.edges(G):
        # print(Y.y[e[0]][k],Y.y[e[1]][k])
        if m:
            plt.plot([P_init[0][dimension_espace * e[0]], P_init[0][dimension_espace * e[1]]],
                     [P_init[0][(dimension_espace * e[0]) + 1], P_init[0][(dimension_espace * e[1]) + 1]], 'b',
                     label="Position inital")
            plt.plot([P_desire[0][dimension_espace * e[0]], P_desire[0][dimension_espace * e[1]]],
                     [P_desire[0][(dimension_espace * e[0]) + 1], P_desire[0][(dimension_espace * e[1]) + 1]], 'y',
                     label="Position final")
            m = False

        plt.plot([P_init[0][dimension_espace * e[0]], P_init[0][dimension_espace * e[1]]],
                 [P_init[0][(dimension_espace * e[0]) + 1], P_init[0][(dimension_espace * e[1]) + 1]], 'b')
        plt.plot([P_desire[0][dimension_espace * e[0]], P_desire[0][dimension_espace * e[1]]],
                 [P_desire[0][(dimension_espace * e[0]) + 1], P_desire[0][(dimension_espace * e[1]) + 1]], 'y')
        # lines[m].set_color('r')
        # lines[m].set_linestyle(":")
        # lines[k].set
        # m = m+1
    plt.xlabel("x(m)")
    plt.ylabel("y(m)")
    legend = plt.legend()
    # plt.show()
    return lines + [legend]


def animate(k):
    legend = plt.legend()
    L_pos_final = []
    j = 0
    for i in range(0, dimension_espace * Nb_Agents, dimension_espace):
        lines[j].set_data(Y.y[i][0:k], Y.y[i + 1][0:k])  # , label='Agent ' + str(i // Nb_Agents + i % Nb_Agents))
        lines[j].set_label('Agent ' + str(i // Nb_Agents + i % Nb_Agents))
        # L_pos_final.append([Y.y[i][k], Y.y[i + 1][k]])
        j = j + 1
    # P_final = np.array(L_pos_final)
    # fermeture_final = np.vstack((P_final, P_final[0])).T
    m = j
    for e in nx.edges(G):
        # print(Y.y[e[0]][k],Y.y[e[1]][k])
        lines[m].set_data([Y.y[dimension_espace * e[0]][k], Y.y[dimension_espace * e[1]][k]],
                          [Y.y[(dimension_espace * e[0]) + 1][k], Y.y[(dimension_espace * e[1]) + 1][k]])
        lines[m].set_color('r')
        lines[m].set_linestyle(":")
        # lines[k].set
        m = m + 1
    lines[j].set_label("Géometrique a t  = " + str(T[k]))

    # lines[-1].set_data(fermeture_final[0], fermeture_final[1])  # , label="Géometrique a t  = " + str(T[k]))
    # lines[-1].set_label("Géometrique a t  = " + str(T[k]))
    # lines[-1].set_linestyle(":")
    legend.remove()
    legend = plt.legend()
    return lines + [legend]


ani = animation.FuncAnimation(fig, animate, init_func=init, frames=len(T), blit=True, interval=200, repeat=False)
plt.show()


def enregistrement(nom):
    ani.save(nom + ".mp4")



enregistrement("unicycle_0_leader_4agents_bearing_opti2")
