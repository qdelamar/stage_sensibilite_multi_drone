import time
from random import uniform

import networkx as nx


from matplotlib import animation
from scipy.integrate import solve_ivp
from sympy import init_printing
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation

from Etape0.ode_helpers import state_plotter

init_printing(use_unicode=True)

print("----------------- Prérequis Graph + parametres --------------------------")
#### graph #####

G = nx.Graph()
G.add_nodes_from([0, 1,2,3,4,5,6,7])
G.add_edges_from([(0, 1),(0,3),(0,4),(1,2),(1,5),(2,3),(2,6),(2,4),(3,7),(4,5),(5,6),(6,7),(4,7)])


#L = nx.laplacian_matrix(G)
#L = L.toarray()
Agents = [n for n in G.nodes()]
Nb_Agents = len(Agents)
dimension_espace = 3
plt.figure(4)
nx.draw(G, with_labels=True)  # affichage graph
plt.show()
L = []

for i in range(Nb_Agents):
    l = []
    if i == 0 or i ==1:
        if i ==0 :
            l = [0,0,0]
        else :
            l = [1, 0, 0]

    else :
        for j in range(dimension_espace):
            l.append(uniform(-2, 2))
        # print(l)
    # print(i)
    L.append(l)


#P = np.array([[1 / np.sqrt(2), 1 / np.sqrt(2)], [-1 / np.sqrt(2), 1 / np.sqrt(2)], [-1 / np.sqrt(2), -1 / np.sqrt(2)],
#              [1 / np.sqrt(2), -1 / np.sqrt(2)]])
Pd = np.array([[0,0,0],[1,0,0],[1,1,0],[0,1,0],[0,0,1],[1,0,1],[1,1,1],[0,1,1]])
P = np.array(L)
print(P)
#(ligne,col) = P.shape
""" 
for i in range(ligne):
    for j in range(col):
        #print(P[i,j])
        P[i,j] = float(P[i,j]+uniform(-1,1))
        print(P[i, j])

print(P)
#P = P + uniform(-1,1)
"""

Tfinal = 200
T = np.linspace(0, Tfinal, 1000)


def controle(t, P):
    try:
        P[0, :]
    except:
        #print(P)
        P = P.reshape((Nb_Agents, dimension_espace))
    affichage = False
    Pp = np.zeros((Nb_Agents, dimension_espace))
    for a in Agents:
        if a == 0 or a==1:
            Pp[a,:] =np.array([0,0,0])
        else :
            Voisins = [l for l in nx.neighbors(G, a)]
            for v in Voisins:
                eij = P[v, :] - P[a, :]
                E = -np.array([[e] for e in eij])
                #gij = E / (np.linalg.norm(E))

                eij_d = (Pd[v, :] - Pd[a, :])
                E2 = np.array([[e] for e in eij_d])
                gij_d = E2 / (np.linalg.norm(E2))
                Pgij_d = np.eye(len(gij_d)) - np.dot(gij_d, gij_d.T)
                # print(np.dot(Pgij, gij_d).T)
                Pp[a, :] = Pp[a, :] - np.dot(Pgij_d, E).T
                if affichage:
                    print('eij : ', eij)
                    print("gij : ", gij)
                    print("np.dot(gij.T,gij) : ", np.dot(gij, gij.T))
                    print("Pgij : ", Pgij)
                    print('gij_d : ', gij_d)
                    # print("Pp[", a, ",:] : ", Pp[a, :])
    Ppv = Pp.reshape((1, Nb_Agents * dimension_espace))
    if affichage:
        print('Pp :', Pp)
        print('Ppv : ', Ppv)
    return Ppv[0]


#C = controle(0, P)


# Y = solve_ivp(controle,[0,20],P.reshape((1,4))[0])

def calcul_affichage_papier():
    start_time = time.time()
    Y = solve_ivp(controle, [0, Tfinal], P.reshape((1, Nb_Agents * dimension_espace))[0], t_eval=T)
    print("temps d'éxecution : %s " % (time.time() - start_time))
    state_plotter(Y.t, Y.y, 1)
    """ 
    plt.show()
    plt.figure(2)
    P_init = P.reshape((Nb_Agents, dimension_espace))
    P_desire = Pd.reshape((Nb_Agents, dimension_espace))
    plt.plot(P_init.T[0], P_init.T[-1], '>', label="Position initial")
    plt.plot(P_desire.T[0], P_desire.T[1], "<", label="Position désiré")
    fermeture_initial = np.vstack((P_init, P_init[0])).T
    fermeture_desire = np.vstack((P_desire, P_desire[0])).T
    
    L_pos_final = []
    for i in range(0, 2 * Nb_Agents, 2):
        plt.plot(Y.y[i], Y.y[i + 1], label='Agent ' + str(i // Nb_Agents + i % Nb_Agents))
        L_pos_final.append([Y.y[i][-1], Y.y[i + 1][-1]])
    P_final = np.array(L_pos_final)
    fermeture_final = np.vstack((P_final, P_final[0])).T
    plt.plot(fermeture_initial[0], fermeture_initial[1])
    plt.plot(fermeture_desire[0], fermeture_desire[1])
    plt.plot(fermeture_final[0], fermeture_final[1], ":*", label="Géometrique final")

    plt.xlabel("x(m)")
    plt.ylabel("y(m)")
    plt.legend()
    plt.show()
    """
    return Y


Y = calcul_affichage_papier()




fig = plt.figure()
ax = p3.Axes3D(fig)
lines = [ax.plot([1], [1],[1])[0] for _ in range(Nb_Agents + len ([e for e in nx.edges(G)]))]
legend = plt.legend()
P_init = P.reshape((1, dimension_espace*Nb_Agents))
#print(P_init)
P_desire = Pd.reshape((1, dimension_espace*Nb_Agents))
#print(P_desire)
#lines[0].set_data(P_init.T[0], P_init.T[1])
#lines[0].set_3d_properties(P_init.T[2])
#ax.plot(P.T[0], P.T[1],P.T[2], '>', label="Position initial")
e = (0,1)
ax.plot([P_init[0][dimension_espace * e[0]], P_init[0][dimension_espace * e[1]]], [P_init[0][(dimension_espace * e[0]) + 1], P_init[0][(dimension_espace * e[1]) + 1]],[P_init[0][(dimension_espace * e[0])+2], P_init[0][(dimension_espace * e[1])+2]],'b',label = "Position inital")
ax.legend()
def init():

    P_init = P.reshape((1, dimension_espace*Nb_Agents))
    #print(P_init)
    P_desire = Pd.reshape((1, dimension_espace*Nb_Agents))
    #print(P_desire)
    ax.plot(P.T[0], P.T[1], P.T[2], '>', label="Position initial")
    ax.plot(Pd.T[0], Pd.T[1], Pd.T[2], "<", label="Position désiré")
    #fermeture_initial = np.vstack((P_init, P_init[0])).T
    #fermeture_desire = np.vstack((P_desire, P_desire[0])).T
    #plt.plot(fermeture_initial[0], fermeture_initial[1])
    #plt.plot(fermeture_desire[0], fermeture_desire[1])
    m = True
    for e in nx.edges(G):
        #print(Y.y[e[0]][k],Y.y[e[1]][k])
        if m :
            ax.plot([P_init[0][dimension_espace * e[0]], P_init[0][dimension_espace * e[1]]], [P_init[0][(dimension_espace * e[0]) + 1], P_init[0][(dimension_espace * e[1]) + 1]],[P_init[0][(dimension_espace * e[0])+2], P_init[0][(dimension_espace * e[1])+2]],'b',label = "geometrie inital")
            ax.plot([P_desire[0][dimension_espace * e[0]], P_desire[0][dimension_espace * e[1]]],[P_desire[0][(dimension_espace * e[0]) + 1], P_desire[0][(dimension_espace * e[1]) + 1]],[P_desire[0][(dimension_espace * e[0]) + 2], P_desire[0][(dimension_espace * e[1]) + 2]], 'y',label = "geometrie final")
            m = False

        ax.plot([P_init[0][dimension_espace * e[0]], P_init[0][dimension_espace * e[1]]],
                [P_init[0][(dimension_espace * e[0]) + 1], P_init[0][(dimension_espace * e[1]) + 1]],
                [P_init[0][(dimension_espace * e[0]) + 2], P_init[0][(dimension_espace * e[1]) + 2]], 'b')
        ax.plot([P_desire[0][dimension_espace * e[0]], P_desire[0][dimension_espace * e[1]]],
                [P_desire[0][(dimension_espace * e[0]) + 1], P_desire[0][(dimension_espace * e[1]) + 1]],
                [P_desire[0][(dimension_espace * e[0]) + 2], P_desire[0][(dimension_espace * e[1]) + 2]], 'y')
        #lines[m].set_color('r')
        #lines[m].set_linestyle(":")
        #lines[k].set
        #m = m+1
    ax.set_xlabel("x(m)")
    ax.set_ylabel("y(m)")
    ax.set_zlabel("z(m)")

    legend = plt.legend()
    #plt.show()
    return lines + [legend]

init()

def animate(k):
    legend = plt.legend()
    L_pos_final = []
    j = 0
    for i in range(0, dimension_espace * Nb_Agents, dimension_espace):
        lines[j].set_data(Y.y[i][0:k], Y.y[i + 1][0:k]) # , label='Agent ' + str(i // Nb_Agents + i % Nb_Agents))
        lines[j].set_3d_properties(Y.y[i+2][0:k])
        lines[j].set_label('Agent ' + str(i // Nb_Agents + i % Nb_Agents))
        #L_pos_final.append([Y.y[i][k], Y.y[i + 1][k]])
        j = j + 1
    #P_final = np.array(L_pos_final)
    #fermeture_final = np.vstack((P_final, P_final[0])).T

    m = j
    for e in nx.edges(G):
        #print(Y.y[e[0]][k],Y.y[e[1]][k])
        lines[m].set_data([Y.y[dimension_espace*e[0]][k],Y.y[dimension_espace*e[1]][k]],[Y.y[(dimension_espace*e[0])+1][k],Y.y[(dimension_espace*e[1])+1][k]])
        lines[m].set_3d_properties([Y.y[(dimension_espace*e[0])+2][k],Y.y[(dimension_espace*e[1])+2][k]])
        lines[m].set_color('r')
        lines[m].set_linestyle(":")
        #lines[k].set
        m = m+1
    lines[j].set_label("Géometrique a t  = " + str(T[k]))



    #lines[-1].set_data(fermeture_final[0], fermeture_final[1])  # , label="Géometrique a t  = " + str(T[k]))
    #lines[-1].set_label("Géometrique a t  = " + str(T[k]))
    #lines[-1].set_linestyle(":")
    legend.remove()
    legend = plt.legend()
    return lines + [legend]


ani = animation.FuncAnimation(fig, animate, init_func=init, frames=len(T), blit=True, interval=50, repeat=False)
plt.show()
def enregistrement(nom):
    ani.save(nom+".mp4")
#enregistrement("traj3D_2leader_fixe_eq9")
# affichage_dynamique(Y)
