import time

import networkx as nx
import numpy as np
import pylab as plt
from scipy.integrate import solve_ivp
from sympy import init_printing

from Etape0.ode_helpers import state_plotter

init_printing(use_unicode=True)

print("----------------- Prérequis Graph + parametres --------------------------")
#### graph #####

G = nx.Graph()
G.add_nodes_from([0, 1])
G.add_edges_from([(0, 1)])

L = nx.laplacian_matrix(G)
L = L.toarray()
Agents = [n for n in G.nodes()]

P = np.array([[1 / np.sqrt(2), 1 / np.sqrt(2)], [-1 / np.sqrt(2), -1 / np.sqrt(2)]])
Pd = np.array([[-1, 0], [1, 0]])


def controle(t, P):
    try:
        P[0, :]
    except:
        P = P.reshape((2, 2))
    affichage = True
    Pp = np.zeros((2, 2))
    for a in Agents:
        Voisins = [l for l in nx.neighbors(G, a)]
        for v in Voisins:
            eij = P[v, :] - P[a, :]
            E = np.array([[eij[0]], [eij[1]]])
            gij = E / (np.linalg.norm(E))
            Pgij = np.eye(len(gij)) - np.dot(gij, gij.T)
            eij_d = (Pd[v, :] - Pd[a, :])
            E2 = np.array([[eij_d[0]], [eij_d[1]]])
            gij_d = E2 / (np.linalg.norm(E2))
            print(np.dot(Pgij, gij_d).T)
            Pp[a, :] = np.dot(Pgij, gij_d).T[0]
            if affichage:
                print('eij : ', eij)
                print("gij : ", gij)
                print("np.dot(gij.T,gij) : ", np.dot(gij, gij.T))
                print("Pgij : ", Pgij)
                print('gij_d : ', gij_d)
                print("Pp[", a, ",:] : ", Pp[a, :])
    Ppv = Pp.reshape((1, 4))
    if affichage:
        print('Pp :', Pp)
        print('Ppv : ', Ppv)
    return Ppv[0]


C = controle(0, P)


# Y = solve_ivp(controle,[0,20],P.reshape((1,4))[0])

def calcul_affichage_papier():
    start_time = time.time()
    Y = solve_ivp(controle, [0, 20], P.reshape((1, 4))[0])
    print("temps d'éxecution : %s " % (time.time() - start_time))
    state_plotter(Y.t, Y.y, 1)
    plt.show()
    plt.figure(2)
    P_init = P.reshape((2, 2))
    P_final = Pd.reshape((2, 2))
    plt.plot(P_init.T[0], P_init.T[-1], '>', label="Position initial")
    plt.plot(P_final.T[0], P_final.T[1], "<", label="Position désiré")
    plt.plot(Y.y[0], Y.y[1])
    plt.plot(Y.y[2], Y.y[3])
    plt.xlabel("x(m)")
    plt.ylabel("y(m)")
    plt.legend()
    plt.show()
    return Y


Y = calcul_affichage_papier()
