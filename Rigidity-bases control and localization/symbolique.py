import time

import networkx as nx
import numpy as np
import pylab as plt
from numpy.polynomial.polynomial import polyval
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
from sympy import init_printing, Matrix, ones, lambdify, MatrixSymbol, BlockMatrix, Derivative, latex, symbols, \
    simplify, zeros

from Etape0.ode_helpers import state_plotter

init_printing(use_unicode=True)

print("----------------- Prérequis Graph + parametres --------------------------")
#### graph #####

G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4])  # ,6,7,8,9])
G.add_edges_from([(1, 2), (1, 4), (2, 4), (2, 3), (3, 4)])  # ,(1,9),(7,8),(5,8),(6,7)])

L = nx.laplacian_matrix(G)
L = L.toarray()
Agents = [n for n in G.nodes()]
#nx.draw(G, with_labels=True)  # affichage graph
#plt.show()

x = symbols('x0:8',real = True)
y = symbols('y0:8',real = True)
xd = symbols('xd0:8',real = True)
yd = symbols('yd0:8',real = True)

P = Matrix([[x[0],y[0]],[x[1],y[1]],[x[2],y[2]],[x[3],y[3]]])
pd = symbols('pd0:8',real = True)
Pd = Matrix([[xd[0],yd[0]],[xd[1],yd[1]],[xd[2],yd[2]],[xd[3],yd[3]]])
print(P)


L =[]
for a in Agents:
    Voisins = [l for l in nx.neighbors(G,a)]
    for v in Voisins:
        if v>a :
            #print(a,v)
            M = P[a-1,:].T-P[v-1,:].T
            #print(M)
            M2 = M.norm()
            #print(M2)
            L.append(M2**2)

#print(L)
pp=zeros(4,2)
for a in Agents:
    Voisins = [l for l in nx.neighbors(G,a)]
    #print(a)
    for v in Voisins:
        #print(a,v)
        eij = P[a-1,:].T-P[v-1,:].T
        #print(eij)
        #print(eij.norm()**2)
        dij = Pd[v-1,:].T-Pd[a-1,:].T
        #print(dij)
        A = (((eij.norm()**2)-(dij.norm()**2))*eij)
        #print(A)
        #print(latex(A,mode="equation"))
        pp[a-1,:] = pp[a-1,:] + A.T
    #print(latex(simplify(-pp[a-1,:].T),mode="equation"))

#print(pp)
#print(latex(pp.reshape(8,1),mode="equation"))
controle  = lambdify([P,Pd],pp,'numpy')
P = np.array([4.5,7,0,4.5,3,3.5,8,1.5])
Pd = np.array([2,2,2,4,4,4,4,2])
C = -controle(P,Pd)
Cv = C.reshape((8,1))
#print(Cv)

def integration(t,P):
    C = -controle(P, Pd)
    Cv = C.reshape((1,8))[0]
    #print(C)
    return Cv
Tfinal = 20
T = np.linspace(0,Tfinal,1000)
#print(integration(0,P))

def controle_numerique_cours(t,P):
    print('P : ' ,P)
    pp = np.zeros((4,2))
    P = P.reshape((4,2))
    print(P)
    Pd2 = Pd.reshape((4,2))
    for a in Agents:
        Voisins = [l for l in nx.neighbors(G, a)]
        # print(a)
        for v in Voisins:
            print('agents : ',a,'voisins : ',v)
            eij = P[a - 1, :].T - P[v - 1, :].T
            print("eij : ",eij)
            # print(eij.norm()**2)
            dij = Pd2[v - 1, :].T - Pd2[a - 1, :].T
            print("dij : ",dij)
            A = (((np.linalg.norm(eij)**2) - (np.linalg.norm(dij)** 2)) * eij)
            print("A = ",A.T)
            # print(latex(A,mode="equation"))
            pp[a - 1, :] = pp[a - 1, :] - A
    print('Pp : ',pp)
    ppv = pp.reshape((1,8))[0]
    print('ppv = ',ppv)
    return ppv

#C = controle_numerique(0,P)
#print(C)
print(P)
#Y = solve_ivp(controle_numerique,[0,Tfinal],P)







def calcul_affichage_cours():
    start_time = time.time()
    Y = solve_ivp(controle_numerique_cours, [0, Tfinal], P,t_eval=T)
    print("temps d'éxecution : %s " % (time.time() - start_time))
    state_plotter(Y.t, Y.y, 1)
    plt.show()
    plt.figure(2)
    P_init = P.reshape((4,2))
    P_final = Pd.reshape((4,2))
    plt.plot(P_init.T[0],P_init.T[-1],'>',label  = "Position initial")
    plt.plot(P_final.T[0],P_final.T[1],"<",label = "Position désiré")
    plt.plot(Y.y[0], Y.y[1])
    plt.plot(Y.y[2], Y.y[3])
    plt.plot(Y.y[4], Y.y[5])
    plt.plot(Y.y[6], Y.y[7])
    plt.legend()
    plt.show()
    return Y,P_init,P_final

#Y,P_init,P_final = calcul_affichage_cours()



def control_papier(t,P):
    print('P : ', P)
    pp = np.zeros((4, 2))
    P = P.reshape((4, 2))
    print(P)
    Pd2 = Pd.reshape((4, 2))
    for a in Agents:
        Voisins = [l for l in nx.neighbors(G, a)]
        # print(a)
        for v in Voisins:
            print('agents : ', a, 'voisins : ', v)
            eij = P[a - 1, :].T - P[v - 1, :].T
            print("eij* : ", eij)
            gij = eij/np.linalg.norm(eij)
            B = 1-np.dot(gij,gij.T)
            eij_d = Pd2[a-1,:].T-Pd2[v-1,:].T
            gij_d = eij_d/np.linalg.norm(eij_d)
            # print(eij.norm()**2)
            #dij = Pd2[v - 1, :].T - Pd2[a - 1, :].T
            #print("dij : ", dij)
            A =B*gij_d
            print("A = ", A.T)
            # print(latex(A,mode="equation"))
            pp[a - 1, :] = pp[a - 1, :] + A
    print('Pp : ', pp)
    ppv = pp.reshape((1, 8))[0]
    print('ppv = ', ppv)
    return ppv

C = control_papier(0,P)
print(C)
print(P)

def calcul_affichage_papier():
    start_time = time.time()
    Y = solve_ivp(control_papier,[0,Tfinal],P)
    print("temps d'éxecution : %s " % (time.time() - start_time))
    state_plotter(Y.t, Y.y, 1)
    plt.show()
    plt.figure(2)
    P_init = P.reshape((4,2))
    P_final = Pd.reshape((4,2))
    plt.plot(P_init.T[0],P_init.T[-1],'>',label  = "Position initial")
    plt.plot(P_final.T[0],P_final.T[1],"<",label = "Position désiré")
    plt.plot(Y.y[0], Y.y[1])
    plt.plot(Y.y[2], Y.y[3])
    plt.plot(Y.y[4], Y.y[5])
    plt.plot(Y.y[6], Y.y[7])
    plt.legend()
    plt.show()
    return Y

#Y= calcul_affichage_papier()

X = Matrix([[x[0]],[y[0]]])
print(X)
print(X*X.T)


""""
g_p_s = Matrix(L)
print(g_p_s)
print(latex(g_p_s,mode='equation'))
print(L)

g = lambdify([P],g_p_s,'numpy')

P0 = np.array([[0,0],[2,5],[5,6],[4,4]])
#print(g(P0))
A = g(P0.reshape((1,8))[0])
print(A)
d = Matrix([0,2,2,0,2,0,0,2])
R_s = simplify(g_p_s.jacobian(P.reshape(8,1)))
print(latex(R_s,mode='equation'))
R = lambdify([P],R_s,'numpy')
print(R(P0.reshape((1,8))[0]))
p_p = R_s.T*(d.T-g_p_s)
#print(latex(p_p,mode='equation'))
B = np.dot(R(P0.reshape((1,8))[0]).T,d.T-A)
print(B)"""