import time

import numpy as np
from scipy.integrate import solve_ivp

from Etape3.ode_helpers import state_plotter

q0 = np.array([0, 0, 0])

def A(theta):
    res = np.array([[np.cos(theta),0], [np.sin(theta),0], [0,1]])
    return res

def u():
    return [1,1]

def dynamique(q,p,u):
    S = np.array([[p[0] / 2, p[0] / 2], [p[0] / (2 * p[1]), -p[0] / (2 * p[1])]])
    m = A(q[-1])
    res = np.dot(np.dot(m,S),u)
    return res

p = [0.05,0.1]

def integrande(t,q):
    return dynamique(q,p,u())


start_time = time.time()
Tfinal = 10
T = np.linspace(0,Tfinal,10000)
Y = solve_ivp(integrande, [0, Tfinal], q0,t_eval=T)
print("temps d'éxecution : %s " % (time.time() - start_time))
state_plotter(Y.t,Y.y,0)