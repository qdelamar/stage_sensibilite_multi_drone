import networkx as nx
import pylab as plt
import numpy as np
G = nx.Graph()
G.add_nodes_from([0,1])
G.add_edges_from([(0,1)])
#nx.draw(G,with_labels=True)
#plt.show()
L = nx.laplacian_matrix(G)
L = L.toarray()
print(L)

X = np.array([4,7])
Y = np.array([2,6])
Theta = np.array([0,1.5])
dx = np.dot(L,X)
dy = np.dot(L,Y)

D = np.sqrt((dx**2)+(dy**2))
phi = np.arctan2(dy,dx)

thetan=Theta-((np.pi/2)-phi)


r = 0.05
b = 0.10
R = np.array([[r/2,r/2],[r/(2*b),-r/(2*b)]])
print(R)
B = np.linalg.inv(R)
C = np.dot(B,np.array(D[0],phi[0]))

