import networkx as nx
import numpy as np
import scipy as sp
from scipy.integrate import solve_ivp
from sympy import init_printing, Matrix, MatrixSymbol, sqrt, atan2, pi, lambdify, latex
import matplotlib as mp
import matplotlib.pyplot as plt

init_printing()
G = nx.Graph()
G.add_nodes_from([0, 1])
G.add_edges_from([(0, 1)])
"""
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])
"""
# nx.draw(G,with_labels=True)
# plt.show()
L = nx.laplacian_matrix(G)
L = L.toarray()
print(L)

Agents = [n for n in G.nodes()]

# nx.draw(G, with_labels=True)  # affichage graph
# plt.show()
# Récupération des scalaires utiles
Nb_param = 2  # p = [m g]
Nb_agents = len(Agents)

Q = Matrix(MatrixSymbol("Q", Nb_agents, 3))
print(Q)
dx = L * Q.col(0)
dy = L * Q.col(1)

# d = sqrt(dx.multiply_elementwise(dx)+dy.multiply_elementwise(dy))
d = Matrix(MatrixSymbol('d', Nb_agents, 1))
Phi = Matrix(MatrixSymbol('phi', Nb_agents, 1))
for i in range(Nb_agents):
    Phi[i] = atan2(dy[i], dx[i])
    d[i] = sqrt((dx[i] ** 2) + (dy[i] ** 2))
up = Matrix(MatrixSymbol('up', Nb_agents * 2, 1))
kc = Matrix(MatrixSymbol('Kc', 2, 1))
print(up)
for i in range(0, Nb_agents * 2, 2):
    print(i)
    up[i] = kc[0] * d[i % Nb_agents]
    up[i + 1] = kc[1] * (Q.col(2)[i % Nb_agents] - ((pi / 2) - Phi[i % Nb_agents]))

print(up)

p = Matrix(MatrixSymbol('P', 2, 1))  # r,b
R = Matrix([[p[0] / 2, p[0] / 2], [p[0] / (2 * p[1]), -p[0] / (2 * p[1])]])
print(R)
U = Matrix(MatrixSymbol('up', Nb_agents * 2, 1))
for i in range(0, Nb_agents * 2, 2):
    r = R * Matrix([[up[i]], [up[i + 1]]])
    U[i] = r[0]
    U[i + 1] = r[1]

print(latex(U, mode="equation"))
entree = lambdify([Q, kc, p], U, "numpy")

Q0 = np.array([[0, 0, 0], [1, 1, 0]])
# ,[3,3,0],[6,6,0],[-2,-2,0]])
KC = [10, 10]
p = [0.1, 0.05]
Q2 = Q0.reshape((3 * Nb_agents, 1))
B = entree(Q2, KC, p)
B = B.reshape((Nb_agents * 2, 1))


def A(theta):
    res = np.array([[np.cos(theta), 0], [np.sin(theta), 0], [0, 1]])
    return res


def dynamique_1agent(q, p, u):
    S = np.array([[p[0] / 2, p[0] / 2], [p[0] / (2 * p[1]), -p[0] / (2 * p[1])]])
    m = A(q[-1])
    # print(np.dot(m,S))
    res = np.dot(np.dot(m, S), u)
    return res


C = dynamique_1agent(Q0[0], p, B[0:2])


def dynamique_multi(Q, p, u):
    res = np.zeros((Nb_agents * 3, 1))
    for i in range(0, Nb_agents * 3, 3):
        print(i)
        print(i % (Nb_agents * 3))
        C = dynamique_1agent(Q[i % (Nb_agents * 3)], p, u[i % (Nb_agents * 3):i % (Nb_agents * 3) + 2])
        print(C)
        res[i] = C[0]
        res[i + 1] = C[1]
        res[i + 2] = C[2]
    return res


# D = dynamique_multi(Q0,p,B)


def dynamique_multi2(Q, p, u):
    S = np.array([[p[0] / 2, p[0] / 2], [p[0] / (2 * p[1]), -p[0] / (2 * p[1])]])
    m = A(Q[0][-1])
    diag = np.dot(m, S)
    for i in range(1, Nb_agents):
        S = np.array([[p[0] / 2, p[0] / 2], [p[0] / (2 * p[1]), -p[0] / (2 * p[1])]])
        m = A(Q[i][-1])
        diag = sp.linalg.block_diag(np.dot(m, S), diag)

    res = np.dot(diag, u)
    return res


D = dynamique_multi2(Q0, p, B)
# print(D)
T_final = 1


def integrande(t, Q):
    if len(Q) == 1:
        u = entree(Q[0], KC, p)
    else:
        u = entree(Q, KC, p)
        # print(u)
    u = u.reshape((Nb_agents * 2, 1))
    Q = Q.reshape((Nb_agents, 3))
    res = dynamique_multi2(Q, p, u)
    res = res.reshape((1, Nb_agents * 3))
    print((t / T_final) * 100)
    return res


KC = np.array([10, 10])
Q0 = Q0.reshape((1, Nb_agents * 3))
print(Q0)
T = np.linspace(0, T_final, 1000)
Y = solve_ivp(integrande, [0, T_final], Q0[0])#, t_eval=T)
print(mp.rcParams['agg.path.chunksize'])
mp.rcParams['agg.path.chunksize'] = 1000000
print(mp.rcParams['agg.path.chunksize'])
fig,ax = plt.subplots()
ax.plot(Y.y[0][:4000],Y.y[1][:4000],'>')
ax.plot(Y.y[3][:4000],Y.y[4][:4000],'r>')
plt.show()

