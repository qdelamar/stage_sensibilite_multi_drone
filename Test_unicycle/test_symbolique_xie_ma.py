import time

import networkx as nx
import numpy as np
from scipy.integrate import solve_ivp
from sympy import init_printing, Matrix, MatrixSymbol, cos, sin, symbols, latex, lambdify, tanh, eye, simplify
from sympy.physics.quantum import TensorProduct
import pylab as plt

from Test_unicycle.ode_helpers import state_plotter

init_printing()
"""
G = nx.Graph()
G.add_nodes_from([0, 1])
G.add_edges_from([(0, 1)])
"""
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4])
G.add_edges_from([(1, 2), (1, 3), (3, 4)])
#"""
# nx.draw(G,with_labels=True)
# plt.show()
L = nx.laplacian_matrix(G)
L = L.toarray()
print(L)

Agents = [n for n in G.nodes()]

# nx.draw(G, with_labels=True)  # affichage graph
# plt.show()
# Récupération des scalaires utiles
Nb_agents = len(Agents)
print(Nb_agents)
Q = Matrix(MatrixSymbol("Q", Nb_agents, 3))
p = Matrix(MatrixSymbol('P', 2, 1))  # r,b
R = Matrix([[p[0] / 2, p[0] / 2], [p[0] / (2 * p[1]), -p[0] / (2 * p[1])]])
print(R)

U = Matrix(MatrixSymbol('U', Nb_agents * 2, 1))
for i in range(Nb_agents):
    if i == 0:
        B = Matrix([[cos(Q[i, 2]), 0], [sin(Q[i, 2]), 0], [0, 1]])# * R
    else:
        B = Matrix.diag(B, Matrix([[cos(Q[i, 2]), 0], [sin(Q[i, 2]), 0], [0, 1]]) )#* R)

f = B * U
k = symbols("k")
p1, p2, p3, p23 = symbols("p1 p2 p3 p23")
P = Matrix([[p1, 0, 0], [0, p2, p23], [0, p23, p3]])
X = Q.reshape(Nb_agents * 3, 1)
h1 = -k * (B.T) * TensorProduct(L, P) * X
eps = symbols("eps")
h2 = -k*tanh((1/eps)*(B.T) * TensorProduct(L, P) * X)
print(latex(h1, mode="equation"))
print(latex(h2,mode="equation"))
entree1 = lambdify([Q, k, p1, p2, p3, p23, p], h1, "numpy")
entree2 = lambdify([Q, k, p1, p2, p3, p23, p,eps], h2, "numpy")
dynamique = lambdify([Q, p, U], f, "numpy")
dynamique_boucle = B*h2
print(latex(dynamique_boucle,mode="equation"))
test = TensorProduct(eye(Nb_agents),P)
test1 = TensorProduct(L,eye(3))
test2 = test*test1
print(latex(test, mode="equation"))
print(latex(test1,mode="equation"))
print(latex(test2,mode="equation"))

print(latex(simplify(test1*X),mode="equation"))
print(latex(simplify(test2*X),mode="equation"))

Q0 = np.array([[0, 0, 0], [2, 0, 0] ,[-2,0,np.pi],[-4,0,np.pi]])
Q0 = Q0.reshape((1,Nb_agents*3))
k = 5
p = [0.3, 0.05]
p1 = 20#12
p2 = 3#1
p3 = 3#2
p23 = -0.5#-1.2
eps = 1#0.3
U_test = entree2(Q0[0], k, p1, p2, p3, p23, p,eps)
D = dynamique(Q0[0],p,U_test)
D = D.reshape((Nb_agents*3,1))
print(D)
Tfinal = 80
def integration(t,Q):
    #print("Q = ",Q)
    Q = Q.reshape((Nb_agents*3,1))
    U = entree2(Q,k,p1,p2,p3,p23,p,eps)
    #print("U = ",U)
    res = dynamique(Q,p,U)
    #print("Dynamique = ",res)
    res = res.reshape((1,Nb_agents*3))
    #print('res = ',res)
    #print((t/Tfinal)*100)
    return res[0]

def calcul():
    start_time = time.time()
    Y = solve_ivp(integration,[0,Tfinal],Q0[0])
    print("temps d'éxecution : %s " % (time.time() - start_time))
    return Y

def affichage():
    state_plotter(Y.t, Y.y, 1)
    plt.show()
    plt.figure(2)
    plt.plot(Y.y[0], Y.y[1])
    plt.plot(Y.y[3], Y.y[4])
    plt.plot(Y.y[6], Y.y[7])
    plt.plot(Y.y[9], Y.y[10])

    plt.show()
Y = calcul()
affichage()