# Stage_Sensibilite_multi_drone

Git : Adrien vigné => Stage 2020   
Sujet :  Génération de trajectoires robustes aux incertitudes paramétriques tenant compte de la commande pour une formation multi-robots  

Etape 0 : prise en main librairie graph et intégration pour cas 1D et test 3D   
Etape 1 : intégration avec position désirée   
Etape 2 : dynamique du quadrotor en controle d'altitude en force   
Etape 3 : Ajout du calcul de sensibilité avec prise en main d'une bibliothèque pour le calcul symbolique   
Etape 4 : Vérification de l'influence du degré de connexion de chaque agent sur la sensibilité    


