from __future__ import division

import time

import networkx as nx
import numpy as np
import pylab as plt
from scipy.integrate import solve_ivp
from sympy import init_printing, Matrix, ones, lambdify, MatrixSymbol, BlockMatrix

init_printing()

###### prérequis : graphe , nb_parametres_c ,nb_parametres ######

print("----------------- Prérequis Graph + parametres --------------------------")
#### graph #####

# test avec 5
G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4, 5])  # ,6,7,8,9])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])  # ,(1,9),(7,8),(5,8),(6,7)])

"""
print("graph flouch")
G = nx.Graph()
G.add_nodes_from([1,2,3,4,5,6,7])
G.add_edges_from([(1,2),(2,5),(2,3),(3,5),(3,4),(4,6),(4,7),(6,7)]) 
"""

# recuperation de la matrice Laplacienne et du nombre d'agents
L = nx.laplacian_matrix(G)
L = L.toarray()
Agents = [n for n in G.nodes()]

nx.draw(G, with_labels=True)  # affichage graph
plt.show()
# Récupération des scalaires utiles
Nb_param = 2  # p = [m g]
Nb_agents = len(L)

print("Matrice laplacienne : ")
print(L)
print("Nb_param : ", Nb_param)
print("Nb_agents : ", Nb_agents)

############ Définition du symbolique ############
print("----------------- Défintion Symbolique --------------------------")
### vecteur parametres reel ###
p = Matrix(MatrixSymbol('p', Nb_param, 1))
### vecteur parametres controleur ###
pc = Matrix(MatrixSymbol('pc', Nb_param, 1))
### vecteur des coef de réglages ###
kc = Matrix(MatrixSymbol('kc', 2, 1))
### definition du vecteur etat ####
Z = Matrix(MatrixSymbol('Z', Nb_agents, 1))
Zp = Matrix(MatrixSymbol('Zp', Nb_agents, 1))
Q = Matrix(BlockMatrix([[Z], [Zp]]))
Z_des = Matrix(MatrixSymbol('Z_des', Nb_agents, 1))
### definition du vecteur d'entrées ###
U = Matrix(MatrixSymbol('U', Nb_agents, 1))
### definition de la matrice de sensibilité ###
Pi = Matrix(MatrixSymbol('Pi', Nb_agents * 2, Nb_param))
### defintion de h ####
h = - kc[0] * L * (Z - Z_des) - kc[1] * L * Zp + pc[0] * pc[1] * ones(Nb_agents, 1)
### definition de f ####
f = Matrix(BlockMatrix([[Zp], [-p[1] * ones(Nb_agents, 1) + U / p[0]]]))
### calcul des jacobiennes ####
df_dq = f.jacobian(Q)
df_dp = f.jacobian(p)
df_du = f.jacobian(U)

dh_dzdes = h.jacobian(Z_des)
dZes_dp = Z_des.jacobian(p)
dh_dq = h.jacobian(Q)
dh_dpc = h.jacobian(pc)
dpc_dp = pc.jacobian(p)
dh_dkc = h.jacobian(kc)
dkc_dp = kc.jacobian(p)

### définition de la sensibilité des entrées ####
Theta = dh_dzdes * dZes_dp + dh_dq * Pi + dh_dpc * dpc_dp + dh_dkc * dkc_dp

Pip = df_dq * Pi + df_du * Theta + df_dp

pip_numerique = lambdify([Q, Z_des, p, pc, kc, Pi, U], Pip, 'numpy')
Qp = lambdify([Q, U, p], f, 'numpy')
entree = lambdify([Q, Z_des, pc, kc], h, "numpy")


def dynamique_entre(t, Q):
    return Qp(Q, entree(Q, Z_des, pc, kc), p)


#### fonction a integrer avec un vecteur d'état étendu pour calcul Pi aussi ###
def intregante(t, Q_bar):
    Q = Q_bar[:Nb_agents * 2]
    Pi = Q_bar[Nb_agents * 2:]
    # print(Pi)
    P1 = Pi
    Pi = Pi.reshape((2 * Nb_agents, Nb_param))
    # print(Pi)
    Qr = dynamique_entre(t, Q)
    Qr = Qr.reshape((1, Nb_agents * 2))
    # print(Q2)
    Pi_r = pip_numerique(Q, Z_des, p, pc, kc, P1, entree(Q, Z_des, pc, kc))
    Pi_r = Pi_r.reshape((1, 2 * Nb_agents * Nb_param))
    Q_bar_r = np.hstack((Qr[0], Pi_r[0]))
    # print(Q_bar_r)
    return Q_bar_r


###  Valeur initial #####
print("------------ Definir les valeurs initiale -------------------")

q0 = np.array([0, 2, 4, 6, 8])  # ,2,-1,8,-5])
qp0 = np.array([0, 0, 0, 0, 0])  # ,0,0,0,0])
Z_des = np.array([4, -2, 3, 0, 15])  # ,0,0,0,0],)
"""
q0 = np.array([1.2,0.5,0.01,1,0.5,2.1,0.7])
qp0 = np.array([0]*Nb_agents)
Z_des = np.array([10,8,5,3,6.5,0,-2])
"""
pc = np.array([2, 9.81])
p = np.array([2, 9.81])
kc = np.array([5, 10])

### mise en forme des données ####
""" forme du vecteur d'etat [z0,...,zi,...,zn,z0p,...,zip,...,znp] avec zi l'altitude de l'agent i et zip sa vitesse """
Pi0 = np.zeros((1, 2 * Nb_agents * Nb_param))
Q = np.concatenate((q0, qp0))  # mise en forme du vecteur etat
Q_bar = np.hstack((Q, Pi0[0]))  # mise en forme des parametres initiaux vecteur etat etendu

### definition des parametres d'intégrations ####
T_final = 30
T = np.linspace(0, T_final, 10000)
### integration ###
start_time = time.time()
Y = solve_ivp(intregante, [0, T_final], Q_bar)  # ,t_eval=T)
print("temps d'éxecution : %s " % (time.time() - start_time))

######## affichage du résultat d'intégrations #############

for i in range(0, Nb_agents):
    plt.figure(3)
    plt.plot(Y.t, Y.y[i], label="q" + str(i))
    plt.legend()
    plt.figure(4)
    plt.plot(Y.t, Y.y[Nb_agents + i], label="qp" + str(i))
    plt.legend()
plt.show()

for k in range(2 * Nb_agents, 3 * Nb_agents):
    for j in range(Nb_param):
        plt.figure(5 + j)
        plt.plot(Y.t, Y.y[k + j], label="dq" + str(k - (2 * Nb_agents)) + "dp" + str(j))
        plt.legend()
plt.show()
for k in range(3 * Nb_agents, 4 * Nb_agents):
    for j in range(Nb_param):
        plt.figure(5 + Nb_param + 1 + j)
        plt.plot(Y.t, Y.y[k + j], label="dqp" + str(k - (3 * Nb_agents)) + "dp" + str(j))
        plt.legend()
plt.show()
"""
# state_plotter(Y.t, Y.y, 4)
fig_dm = plt.figure(4)
fig_dg = plt.figure(5)
k = 1
for j in range(10, 30):
    if (j % 2 == 0):
        if (j < 20):
            legend = "dz" + str(k) + "_dm"
            plt.figure(4)
            plt.plot(Y.t, Y.y[j], label=legend)
            plt.legend()
        else:
            legend = "dzp" + str(k)+"_dm"
            plt.figure(6)
            plt.plot(Y.t, Y.y[j], label=legend)
            plt.legend()

    else:
        if (j < 20):
            legend = "dz" + str(k) + "_dg"
            plt.figure(5)
            plt.plot(Y.t, Y.y[j], label=legend)
            plt.legend()
        else:
            legend = "dzp" + str(k)+"_dg"
            plt.figure(7)
            plt.plot(Y.t, Y.y[j], label=legend)
            plt.legend()
    k = k+1

plt.figure()
"""
