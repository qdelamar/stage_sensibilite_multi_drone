from __future__ import division

import networkx as nx
import numpy as np
from sympy import *

init_printing()
z1, z2, z3, z4, z5, zp1, zp2, zp3, zp4, zp5, zpp1, zpp2, zpp3, zpp4, zpp5 = symbols(
    'z1 z2 z3 z4 z5 zp1 zp2 zp3 zp4 zp5 zpp1 zpp2 zpp3 zpp4 zpp5')
Z = Matrix([z1, z2, z3, z4, z5, zp1, zp2, zp3, zp4, zp5])
Zp = Matrix([zp1, zp2, zp3, zp4, zp5, zpp1, zpp2, zpp3, zpp4, zpp5])
print(Z)
print(Zp)

##Création du graphe
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])

L = nx.laplacian_matrix(G)  # matrice creuse
# print(L)

L = L.toarray()  # matrice complete pour le calcul et l'affichage

print(L)
# nx.draw(G, with_labels=True)
print(G.nodes())
# str_vecteur = "Z=["
str_vecteur_symbolique = "Z_s=Matrix(["
Nodes = [i for i in G.nodes()]
for i in range(len(Nodes)):
    # str_vecteur = str_vecteur + str(Nodes[i])
    str_vecteur_symbolique = str_vecteur_symbolique + "z" + str(Nodes[i])
    if i != len(Nodes) - 1:
        # str_vecteur = str_vecteur + ","
        str_vecteur_symbolique = str_vecteur_symbolique + ","
# str_vecteur = str_vecteur + "]"
str_vecteur_symbolique = str_vecteur_symbolique + "])"
# print(str_vecteur)
print(str_vecteur_symbolique)
# exec(str_vecteur)
exec(str_vecteur_symbolique)

L_s = Matrix(L)
print(L_s)
print(L_s * Z_s)
g = symbols('g')
G = ones(5, 1) * g
k1, k2, m = symbols('K1 K2 m')
# dynamique = Matrix([Z[5:], -G + (k1 / m) * L_s * Z[:5] + (k2 / m) * L_s * Z[5:]])
Zp_s = Matrix(Z[5:])
print(Zp_s)
expr = Matrix.vstack(Zp_s, -G - (k1 / m) * L_s * Z_s - (k2 / m) * L_s * Zp_s)
print(expr)
J = expr.jacobian(Z)
print(latex(J, mode='equation'))
# print(dynamique)
J_z = expr.jacobian(Matrix([m, g]))
print(latex(J_z, mode='equation'))
print(J_z)

DYNAMIQUE_TEST = lambdify((Zp_s, Z_s, m, k2, k1, g), expr, "numpy")
print(type(DYNAMIQUE_TEST))
Z0 = np.array([0, 2, 4, 6, 8])
Z0P = np.array([0, 0, 0, 0, 0])
print(DYNAMIQUE_TEST(Z0P, Z0, 2, 2, 2, 9.81))
print(L * Z0)
