from __future__ import division

import time

import networkx as nx
import numpy as np
import pylab as plt
from numpy.polynomial.polynomial import polyval
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
from sympy import init_printing, Matrix, ones, lambdify, MatrixSymbol, BlockMatrix

init_printing()

print("----------------- Prérequis Graph + parametres --------------------------")
#### graph #####

G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4, 5])  # ,6,7,8,9])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])  # ,(1,9),(7,8),(5,8),(6,7)])
"""

G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4, 5, 6, 7,8,9,10])
G.add_edges_from([(1,2),(2,3),(3,6),(6,10),(10,9),(9,5),(5,4),(4,1),(1,8),(8,7),(7,10),(3,8),(3,7),(7,5),(8,5)])
"""
# recuperation de la matrice Laplacienne et du nombre d'agents
L = nx.laplacian_matrix(G)
L = L.toarray()
Agents = [n for n in G.nodes()]

#nx.draw(G, with_labels=True)  # affichage graph
#plt.show()
# Récupération des scalaires utiles
Nb_param = 2  # p = [m g]
Nb_agents = len(Agents)
Deg_traj = 4
N_iter = 200


print("Matrice laplacienne : ")
print(L)
print("Nb_param : ", Nb_param)
print("Nb_agents : ", Nb_agents)
print("Deg_traj : ", Deg_traj)
print("N_iter max : ", N_iter)
q0 = np.array([0, 2, 4, 6, 8])  # ,2,-1,8,-5])
print("Position initial : ",q0)
qp0 = np.array([0, 0, 0, 0, 0])  # ,0,0,0,0])

#Q_des = np.array([4, -2, 3, 0, 15])  # ,0,0,0,0],) # a redefinir en focntion du temps

pc = np.array([2, 9.81])
p = np.array([2, 9.81])
kc = np.array([5, 10])


### mise en forme des données ####
""" forme du vecteur d'etat [z0,...,zi,...,zn,z0p,...,zip,...,znp] avec zi l'altitude de l'agent i et zip sa vitesse """
Pi0 = np.zeros((1, 2 * Nb_agents * Nb_param))
Q = np.concatenate((q0, qp0))  # mise en forme du vecteur etat
Q_bar = np.hstack((Q, Pi0[0]))  # mise en forme des parametres initiaux vecteur etat etendu
qf = np.array([8, 6, 4, 2, 0])
print("Position final voulu : ",qf)
### definition des parametres d'intégrations ####
T_final = 10
print("Tfinal : ", T_final)
T = np.linspace(0, T_final, 1000)
### integration ###
a = [q0[0], 0, 0, 0, q0[1], 0, 0, 0, q0[2], 0, 0, 0, q0[3], 0, 0, 0, q0[4], 0, 0, 0]
print("valeurs inital polynome: ", a)
T = np.linspace(0, T_final, 1000)


############ Définition du symbolique ############
print("----------------- Défintion Symbolique --------------------------")
### vecteur parametres reel ###
p_s = Matrix(MatrixSymbol('p', Nb_param, 1))
### vecteur parametres controleur ###
pc_s = Matrix(MatrixSymbol('pc', Nb_param, 1))
### vecteur des coef de réglages ###
kc_s = Matrix(MatrixSymbol('kc', 2, 1))
### definition du vecteur etat ####
Z = Matrix(MatrixSymbol('Z', Nb_agents, 1))
Zp = Matrix(MatrixSymbol('Zp', Nb_agents, 1))
Q_s = Matrix(BlockMatrix([[Z], [Zp]]))
Z_des = Matrix(MatrixSymbol('Z_des', Nb_agents, 1))
### definition du vecteur d'entrées ###
U = Matrix(MatrixSymbol('U', Nb_agents, 1))
### definition de la matrice de sensibilité ###
Pi = Matrix(MatrixSymbol('Pi', Nb_agents * 2, Nb_param))
### defintion de h ####
h = - kc_s[0] * L * (Z - Z_des) - kc_s[1] * L * Zp + pc_s[0] * pc_s[1] * ones(Nb_agents, 1)

### definition de f ####
f = Matrix(BlockMatrix([[Zp], [-p_s[1] * ones(Nb_agents, 1) + U / p_s[0]]]))

### calcul des jacobiennes ####
df_dq = f.jacobian(Q_s)
df_dp = f.jacobian(p_s)
df_du = f.jacobian(U)

dh_dzdes = h.jacobian(Z_des)
dZes_dp = Z_des.jacobian(p_s)
dh_dq = h.jacobian(Q_s)
dh_dpc = h.jacobian(pc_s)
dpc_dp = pc_s.jacobian(p_s)
dh_dkc = h.jacobian(kc_s)
dkc_dp = kc_s.jacobian(p_s)

### définition de la sensibilité des entrées ####
Theta = dh_dzdes * dZes_dp + dh_dq * Pi + dh_dpc * dpc_dp + dh_dkc * dkc_dp

Pip = df_dq * Pi + df_du * Theta + df_dp

pip_numerique = lambdify([Q_s, Z_des, p_s, pc_s, kc_s, Pi, U], Pip, 'numpy')
Qp = lambdify([Q_s, U, p_s], f, 'numpy')
entree = lambdify([Q_s, Z_des, pc_s, kc_s], h, "numpy")

print("---------------obtention de la trajectoire non optimisée---------------")

def affichage_resultat(Y,N,titre):
    for i in range(0, Nb_agents):
        plt.figure(N)
        plt.plot(Y.t, Y.y[i], label="q" + str(i + 1))
        plt.title("Positon au cours du temps "+titre)
        plt.legend()
        plt.figure(N+1)
        plt.plot(Y.t, Y.y[Nb_agents + i], label="qp" + str(i + 1))
        plt.title("Vitesse au cours du temps " + titre)
        plt.legend()
    plt.show()

def intregante_non_opti(t, Q_bar):
    Q = Q_bar[:Nb_agents * 2]
    Pi = Q_bar[Nb_agents * 2:]
    # print(Pi)
    P1 = Pi
    Pi = Pi.reshape((2 * Nb_agents, Nb_param))
    # print(Pi)
    U = entree(Q, qf, pc, kc)
    Qr = Qp(Q,U , p)
    Qr = Qr.reshape((1, Nb_agents * 2))
    #print(p)
    # print(Q2)
    Pi_r = pip_numerique(Q, qf, p, pc, kc, P1, U)
    Pi_r = Pi_r.reshape((1, 2 * Nb_agents * Nb_param))
    Q_bar_r = np.hstack((Qr[0], Pi_r[0]))
    # print(Q_bar_r)
    return Q_bar_r


Y = solve_ivp(intregante_non_opti, [0, T_final], Q_bar, t_eval=T)
#affichage_resultat(Y,2,"initial non opti")

def extraction_matrice_norme_tfinal(sol):
    pic = sol.y[Nb_agents * 2:]
    (a, b) = pic.shape
    nb_param = int(a / (Nb_agents * 2))
    pic = pic.reshape((Nb_agents * 2, nb_param, b))
    Mt = []
    Norme = []
    for k in range(b):
        M = np.zeros((Nb_agents * 2, nb_param))
        for i in range(Nb_agents * 2):
            for j in range(nb_param):
                M[i, j] = pic[i, j][k]
        Mt.append(M)
        Norme.append(np.linalg.norm(M))
    return Norme[-1]

def Dynamique_fct_traj(a):
    def Z_des(a, t):
        try:
            res = np.zeros((len(t), Nb_agents))
        except:
            res = np.zeros((1, Nb_agents))
        for i in range(Nb_agents):
            res[:, i] = polyval(t, a[(Deg_traj * i):(Deg_traj * (i + 1))])
        return res
    def intregante(t, Q_bar):
        Q = Q_bar[:Nb_agents * 2]
        Pi = Q_bar[Nb_agents * 2:]
        #print(p)
        P1 = Pi
        Pi = Pi.reshape((2 * Nb_agents, Nb_param))
        # print(Pi)
        Qr = Qp(Q, entree(Q, Z_des(a, t)[0], pc, kc), p)
        Qr = Qr.reshape((1, Nb_agents * 2))
        # print(Q2)
        Pi_r = pip_numerique(Q, Z_des(a, t)[0], p, pc, kc, P1, entree(Q, Z_des(a, t)[0], pc, kc))
        Pi_r = Pi_r.reshape((1, 2 * Nb_agents * Nb_param))
        Q_bar_r = np.hstack((Qr[0], Pi_r[0]))
        # print(Q_bar_r)
        return Q_bar_r

    Y = solve_ivp(intregante, [0, T_final], Q_bar, t_eval=T)

    return Y

def objectif(a):
    Y = Dynamique_fct_traj(a)
    norme = extraction_matrice_norme_tfinal(Y)
    Qf = np.zeros((1, Nb_agents))
    Qpf = np.zeros((1, Nb_agents))
    for j in range(Nb_agents):
        Qf[:, j] = Y.y[j][-1]
        Qpf[:, j] = Y.y[j + Nb_agents][-1]

    return norme # + np.linalg.norm(Qf - qf) + np.linalg.norm(Qpf)

def Z_des(a, t):
    try:
        res = np.zeros((len(t), Nb_agents))
    except:
        res = np.zeros((1, Nb_agents))
    for i in range(Nb_agents):
        res[:, i] = polyval(t, a[(Deg_traj * i):(Deg_traj * (i + 1))])
    return res

def cont_initial(x):
    res = np.linalg.norm(q0 - Z_des(x, 0))
    return res

contrainte_inital = {'type': 'eq', 'fun': cont_initial}
opts = {'disp': True, 'maxiter': N_iter}
start_time = time.time()
a_opti = minimize(objectif, a, method="SLSQP", constraints=contrainte_inital, options=opts)
print("temps d'éxecution : %s " % (time.time() - start_time))
print("La solution opti trouvé est : ",a_opti.x)
Y_opti = Dynamique_fct_traj(a_opti.x.tolist())
Qf = np.zeros((1, Nb_agents))
Qpf = np.zeros((1, Nb_agents))
for j in range(Nb_agents):
    Qf[:, j] = Y_opti.y[j][-1]
    Qpf[:, j] = Y_opti.y[j + Nb_agents][-1]
print(Qf)
print(Qpf)
#affichage_resultat(Y_opti,4," opti")

print("--------------Variations du paramètres de masse-------------------")
print("+/-5% avec 5 points")
M = np.linspace(p[0]*0.95,p[0]*1.05,5,endpoint=True)
Liste_Y_m_opti = []
Liste_Y_m_non_opti = []
Nombre_affichage = 6
for m in M:
    p = [m,p[1]]
    Y_m_opti = Dynamique_fct_traj(a_opti.x.tolist())
    Y_m_non_opti = solve_ivp(intregante_non_opti, [0, T_final], Q_bar, t_eval=T)
    #affichage_resultat(Y_m_opti,Nombre_affichage,"opti avec m = "+str(m))
    #affichage_resultat(Y_m_non_opti,Nombre_affichage+2,"non opti avec m = "+str(m))
    #Nombre_affichage = Nombre_affichage + 4
    Liste_Y_m_opti.append(Y_m_opti)
    Liste_Y_m_non_opti.append(Y_m_non_opti)

def affichage_diff(Liste_Y_m_opti,Liste_Y_m_non_opti):
    Q_agent_opti = []
    Q_agent_non_opti = []
    for n in range(Nb_agents):
        liste_opti = []
        liste_non_opti = []
        for i in range(len(M)):
            liste_opti.append(Liste_Y_m_opti[i].y[n])
            liste_non_opti.append(Liste_Y_m_non_opti[i].y[n])
        Q_agent_opti.append(liste_opti)
        Q_agent_non_opti.append(liste_non_opti)
    return Q_agent_opti,Q_agent_non_opti

Q_m_opti,Q_m_non_opti  = affichage_diff(Liste_Y_m_opti,Liste_Y_m_non_opti)

def affichage_diff_plot(Q_m_opti,Q_m_non_opti):
    for n in range(Nb_agents):
        for i in range(len(M)):
            plt.figure(Nombre_affichage + 4 + n)
            plt.plot(T,Q_m_opti[n][i],label = "Q"+str(n)+"_opti  pour m = "+str(M[i]))
            plt.legend()
            plt.figure(Nombre_affichage + 4 + 2*(n+1))
            plt.plot(T,Q_m_non_opti[n][i],label = "Q"+str(n)+"_non_opti pour m = "+str(M[i]))
            plt.legend()
    plt.show()
affichage_diff_plot(Q_m_opti,Q_m_non_opti)

def calcul_erreur_final(Q_m_opti,Q_m_non_opti,Qf):
    erreur_opti = []
    erreur_non_opti = []
    for n in range(Nb_agents):
        liste_e_opti = []
        liste_e_non_opti = []
        for i in range(len(M)):
            liste_e_opti.append(np.linalg.norm(Q_m_opti[n][i][-1]-Qf[n]))
            liste_e_non_opti.append(np.linalg.norm(Q_m_non_opti[n][i][-1]-Qf[n]))
        erreur_opti.append(liste_e_opti)
        erreur_non_opti.append(liste_e_non_opti)
    return erreur_opti,erreur_non_opti

erreur_opti,erreur_non_opti= calcul_erreur_final(Q_m_opti,Q_m_non_opti,Qf)
print("erreur final opti : ",erreur_opti)
print("erreur final non opti : ",erreur_non_opti)

