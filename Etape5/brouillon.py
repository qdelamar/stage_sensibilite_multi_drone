import numpy as np
from numpy.polynomial import Polynomial as P
from numpy.polynomial.polynomial import polyval
from IPython.display import Latex
import pylab as plt

def prettyprintPolynomial(p):
    """ Small function to print nicely the polynomial p as we write it in maths, in ASCII text."""
    coefs = p.coef  # List of coefficient, sorted by increasing degrees
    res = ""  # The resulting string
    for i, a in enumerate(coefs):
        if int(a) == a:  # Remove the trailing .0
            a = int(a)
        if i == 0:  # First coefficient, no need for X
            if a > 0:
                res += "{a} + ".format(a=a)
            elif a < 0:  # Negative a is printed like (a)
                res += "({a}) + ".format(a=a)
            # a = 0 is not displayed
        elif i == 1:  # Second coefficient, only X and not X**i
            if a == 1:  # a = 1 does not need to be displayed
                res += "X + "
            elif a > 0:
                res += "{a} * X + ".format(a=a)
            elif a < 0:
                res += "({a}) * X + ".format(a=a)
        else:
            if a == 1:
                res += "X**{i} + ".format(i=i)
            elif a > 0:
                res += "{a} * X**{i} + ".format(a=a, i=i)
            elif a < 0:
                res += "({a}) * X**{i} + ".format(a=a, i=i)
    return res[:-3] if res else ""

def Polynomial_to_LaTeX(p):
    """ Small function to print nicely the polynomial p as we write it in maths, in LaTeX code."""
    coefs = p.coef  # List of coefficient, sorted by increasing degrees
    res = ""  # The resulting string
    for i, a in enumerate(coefs):
        if int(a) == a:  # Remove the trailing .0
            a = int(a)
        if i == 0:  # First coefficient, no need for X
            if a > 0:
                res += "{a} + ".format(a=a)
            elif a < 0:  # Negative a is printed like (a)
                res += "({a}) + ".format(a=a)
            # a = 0 is not displayed
        elif i == 1:  # Second coefficient, only X and not X**i
            if a == 1:  # a = 1 does not need to be displayed
                res += "X + "
            elif a > 0:
                res += "{a} \;X + ".format(a=a)
            elif a < 0:
                res += "({a}) \;X + ".format(a=a)
        else:
            if a == 1:
                # A special care needs to be addressed to put the exponent in {..} in LaTeX
                res += "X^{i} + ".format(i="{%d}" % i)
            elif a > 0:
                res += "{a} \;X^{i} + ".format(a=a, i="{%d}" % i)
            elif a < 0:
                res += "({a}) \;X^{i} + ".format(a=a, i="{%d}" % i)
    return "$" + res[:-3] + "$" if res else ""

nb_agents = 5
Deg_traj = 2
def Z_des(a,t):
    if (type(t)==type(0.1)) or (type(t)==type(1)):
        res =  np.zeros((1,nb_agents))
    else :
        res = np.zeros((len(t),nb_agents))
    #print(res)
    #print(res.shape)
    for i in range(nb_agents):
        res[:,i] = polyval(t,a[(Deg_traj*i):(Deg_traj*(i+1))])
        #print((Deg_traj*i),(Deg_traj*(i+1)))
        #print(a[(Deg_traj*i):(Deg_traj*(i+1))])
        #print(res[0,i])
    return res

a = [0,1,0,0,0,0,1,0,0,0]
print(Z_des(a,1))
T = np.linspace(0,10,1000)
R = Z_des(a,T)

def affichage(R,T):
    plt.figure(1)
    for i in range(nb_agents):
        plt.plot(T,R[:,i])
    plt.show()

affichage(R,T)

