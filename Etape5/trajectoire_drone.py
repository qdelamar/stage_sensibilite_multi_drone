from __future__ import division

import time

import networkx as nx
import numpy as np
import pylab as plt
from numpy.polynomial.polynomial import polyval
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
from sympy import init_printing, Matrix, ones, lambdify, MatrixSymbol, BlockMatrix

init_printing()

###### prérequis : graphe , nb_parametres_c ,nb_parametres ######

print("----------------- Prérequis Graph + parametres --------------------------")
#### graph #####

G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4, 5])  # ,6,7,8,9])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])  # ,(1,9),(7,8),(5,8),(6,7)])
"""

G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4, 5, 6, 7,8,9,10])
G.add_edges_from([(1,2),(2,3),(3,6),(6,10),(10,9),(9,5),(5,4),(4,1),(1,8),(8,7),(7,10),(3,8),(3,7),(7,5),(8,5)])
"""
# recuperation de la matrice Laplacienne et du nombre d'agents
L = nx.laplacian_matrix(G)
L = L.toarray()
Agents = [n for n in G.nodes()]

# nx.draw(G, with_labels=True)  # affichage graph
# plt.show()
# Récupération des scalaires utiles
Nb_param = 2  # p = [m g]
Nb_agents = len(Agents)
Deg_traj = 4

print("Matrice laplacienne : ")
print(L)
print("Nb_param : ", Nb_param)
print("Nb_agents : ", Nb_agents)
print("Deg_traj : ", Deg_traj)

############ Définition du symbolique ############
print("----------------- Défintion Symbolique --------------------------")
### vecteur parametres reel ###
p = Matrix(MatrixSymbol('p', Nb_param, 1))
### vecteur parametres controleur ###
pc = Matrix(MatrixSymbol('pc', Nb_param, 1))
### vecteur des coef de réglages ###
kc = Matrix(MatrixSymbol('kc', 2, 1))
### definition du vecteur etat ####
Z = Matrix(MatrixSymbol('Z', Nb_agents, 1))
Zp = Matrix(MatrixSymbol('Zp', Nb_agents, 1))
Q = Matrix(BlockMatrix([[Z], [Zp]]))
Z_des = Matrix(MatrixSymbol('Z_des', Nb_agents, 1))
### definition du vecteur d'entrées ###
U = Matrix(MatrixSymbol('U', Nb_agents, 1))
### definition de la matrice de sensibilité ###
Pi = Matrix(MatrixSymbol('Pi', Nb_agents * 2, Nb_param))
### defintion de h ####
h = - kc[0] * L * (Z - Z_des) - kc[1] * L * Zp + pc[0] * pc[1] * ones(Nb_agents, 1)

### definition de f ####
f = Matrix(BlockMatrix([[Zp], [-p[1] * ones(Nb_agents, 1) + U / p[0]]]))

### calcul des jacobiennes ####
df_dq = f.jacobian(Q)
df_dp = f.jacobian(p)
df_du = f.jacobian(U)

dh_dzdes = h.jacobian(Z_des)
dZes_dp = Z_des.jacobian(p)
dh_dq = h.jacobian(Q)
dh_dpc = h.jacobian(pc)
dpc_dp = pc.jacobian(p)
dh_dkc = h.jacobian(kc)
dkc_dp = kc.jacobian(p)

### définition de la sensibilité des entrées ####
Theta = dh_dzdes * dZes_dp + dh_dq * Pi + dh_dpc * dpc_dp + dh_dkc * dkc_dp

Pip = df_dq * Pi + df_du * Theta + df_dp

pip_numerique = lambdify([Q, Z_des, p, pc, kc, Pi, U], Pip, 'numpy')
Qp = lambdify([Q, U, p], f, 'numpy')
entree = lambdify([Q, Z_des, pc, kc], h, "numpy")


def dynamique_entre(t, Q):
    return Qp(Q, entree(Q, Q_des, pc, kc), p)


def Z_des(a, t):
    """
    if (type(t)==type(0.1)) or (type(t)==type(1)):
        res =  np.zeros((1,Nb_agents))
    else :
        res = np.zeros((len(t),Nb_agents))
     """

    try:
        res = np.zeros((len(t), Nb_agents))
    except:
        res = np.zeros((1, Nb_agents))
    # print(res)
    # print(res.shape)
    for i in range(Nb_agents):
        res[:, i] = polyval(t, a[(Deg_traj * i):(Deg_traj * (i + 1))])
        # print((Deg_traj*i),(Deg_traj*(i+1)))
        # print(a[(Deg_traj*i):(Deg_traj*(i+1))])
        # print(res[0,i])
    return res


def affichage(R, T):
    plt.figure()
    for i in range(Nb_agents):
        plt.plot(T, R[:, i])
    plt.show()


#### fonction a integrer avec un vecteur d'état étendu pour calcul Pi aussi ###
def intregante(t, Q_bar):
    Q = Q_bar[:Nb_agents * 2]
    Pi = Q_bar[Nb_agents * 2:]
    # print(Pi)
    P1 = Pi
    Pi = Pi.reshape((2 * Nb_agents, Nb_param))
    # print(Pi)
    Qr = Qp(Q, entree(Q, Z_des(a, t)[0], pc, kc), p)
    Qr = Qr.reshape((1, Nb_agents * 2))
    # print(Q2)
    Pi_r = pip_numerique(Q, Z_des(a, t)[0], p, pc, kc, P1, entree(Q, Z_des(a, t)[0], pc, kc))
    Pi_r = Pi_r.reshape((1, 2 * Nb_agents * Nb_param))
    Q_bar_r = np.hstack((Qr[0], Pi_r[0]))
    # print(Q_bar_r)
    return Q_bar_r


###  Valeur initial #####
print("------------ Definir les valeurs initiale -------------------")

q0 = np.array([0, 2, 4, 6, 8])  # ,2,-1,8,-5])

qp0 = np.array([0, 0, 0, 0, 0])  # ,0,0,0,0])
Q_des = np.array([4, -2, 3, 0, 15])  # ,0,0,0,0],) # a redefinir en focntion du temps
"""
q0 = np.array([1,2,3,2,3,4,3.5,2.5,4,5])
#qp0 = np.array([0.5,0,0,1,1,0,0.5,0.5,1,0.5])
qp0 = np.array([0]*Nb_agents)
Q_des = np.array([0] * Nb_agents)
"""
pc = np.array([2, 9.81])
p = np.array([2, 9.81])
kc = np.array([5, 10])

### mise en forme des données ####
""" forme du vecteur d'etat [z0,...,zi,...,zn,z0p,...,zip,...,znp] avec zi l'altitude de l'agent i et zip sa vitesse """
Pi0 = np.zeros((1, 2 * Nb_agents * Nb_param))
Q = np.concatenate((q0, qp0))  # mise en forme du vecteur etat
Q_bar = np.hstack((Q, Pi0[0]))  # mise en forme des parametres initiaux vecteur etat etendu
qf = np.array([8, 6, 4, 2, 0])
### definition des parametres d'intégrations ####
T_final = 10
print("Tfinal : ", T_final)
T = np.linspace(0, T_final, 1000)
### integration ###
a = [q0[0], 0, 0, 0, q0[1], 0, 0, 0, q0[2], 0, 0, 0, q0[3], 0, 0, 0, q0[4], 0, 0, 0]
print("valeurs inital polynome: ", a)
T = np.linspace(0, T_final, 1000)
R = Z_des(a, T)


# affichage(R,T)
######## affichage du résultat d'intégrations #############
def affichage_resultat(Y):
    for i in range(0, Nb_agents):
        plt.figure(3)
        plt.plot(Y.t, Y.y[i], label="q" + str(i + 1))
        plt.legend()
        plt.figure(4)
        plt.plot(Y.t, Y.y[Nb_agents + i], label="qp" + str(i + 1))
        plt.legend()
    plt.show()


"""
    for k in range(2 * Nb_agents, 3 * Nb_agents):
        for j in range(Nb_param):
            plt.figure(5 + j)
            plt.plot(Y.t, Y.y[k + j], label="dq" + str(k + 1 - (2 * Nb_agents)) + "dp" + str(j))
            plt.legend()
    plt.show()
    for k in range(3 * Nb_agents, 4 * Nb_agents):
        for j in range(Nb_param):
            plt.figure(5 + Nb_param + 1 + j)
            plt.plot(Y.t, Y.y[k + j], label="dqp" + str(k + 1 - (3 * Nb_agents)) + "dp" + str(j))
            plt.legend()
    plt.show()
"""


def extraction_matrice_norme_tfinal(sol):
    pic = sol.y[Nb_agents * 2:]
    (a, b) = pic.shape
    nb_param = int(a / (Nb_agents * 2))
    pic = pic.reshape((Nb_agents * 2, nb_param, b))
    Mt = []
    Norme = []
    for k in range(b):
        M = np.zeros((Nb_agents * 2, nb_param))
        for i in range(Nb_agents * 2):
            for j in range(nb_param):
                M[i, j] = pic[i, j][k]
        Mt.append(M)
        Norme.append(np.linalg.norm(M))
    return Norme[-1]


def fct_a(a):
    def Z_des(a, t):
        """
        if (type(t)==type(0.1)) or (type(t)==type(1)):
            res =  np.zeros((1,Nb_agents))
        else :
            res = np.zeros((len(t),Nb_agents))
        """

        try:
            res = np.zeros((len(t), Nb_agents))
        except:
            res = np.zeros((1, Nb_agents))
        # print(res)
        # print(res.shape)
        for i in range(Nb_agents):
            res[:, i] = polyval(t, a[(Deg_traj * i):(Deg_traj * (i + 1))])
            # print((Deg_traj*i),(Deg_traj*(i+1)))
            # print(a[(Deg_traj*i):(Deg_traj*(i+1))])
            # print(res[0,i])
        return res

    def intregante(t, Q_bar):
        Q = Q_bar[:Nb_agents * 2]
        Pi = Q_bar[Nb_agents * 2:]
        # print(Pi)
        P1 = Pi
        Pi = Pi.reshape((2 * Nb_agents, Nb_param))
        # print(Pi)
        Qr = Qp(Q, entree(Q, Z_des(a, t)[0], pc, kc), p)
        Qr = Qr.reshape((1, Nb_agents * 2))
        # print(Q2)
        Pi_r = pip_numerique(Q, Z_des(a, t)[0], p, pc, kc, P1, entree(Q, Z_des(a, t)[0], pc, kc))
        Pi_r = Pi_r.reshape((1, 2 * Nb_agents * Nb_param))
        Q_bar_r = np.hstack((Qr[0], Pi_r[0]))
        # print(Q_bar_r)
        return Q_bar_r

    Y = solve_ivp(intregante, [0, T_final], Q_bar, t_eval=T)
    return Y


def cout_initial(x):
    res = np.linalg.norm(q0 - Z_des(x, 0))
    return res


def cout_final(x):
    res = np.linalg.norm(qf - Z_des(x, T_final))
    return res


def objectif(x):
    Y = fct_a(x)
    norme = extraction_matrice_norme_tfinal(Y)
    Qf = np.zeros((1, Nb_agents))
    Qpf = np.zeros((1, Nb_agents))
    for j in range(Nb_agents):
        Qf[:, j] = Y.y[j][-1]
        Qpf[:, j] = Y.y[j + Nb_agents][-1]

    return norme


contrainte_inital = {'type': 'eq', 'fun': cout_initial}
# contrainte_final = {'type' : 'eq','fun': cout_final}
N_iter = 200
print("N_iter max : ", N_iter)
opts = {'disp': True, 'maxiter': N_iter}

start_time = time.time()
a_min = minimize(objectif, a, method="SLSQP", constraints=contrainte_inital, options=opts)
print("temps d'éxecution : %s " % (time.time() - start_time))
print(a_min.x)
Y = fct_a(a_min.x)
# print(cout_initial(a_min.x))
Qf = np.zeros((1, Nb_agents))
Qpf = np.zeros((1, Nb_agents))
for j in range(Nb_agents):
    Qf[:, j] = Y.y[j][-1]
    Qpf[:, j] = Y.y[j + Nb_agents][-1]
print(Qf)
print(Qpf)
affichage_resultat(Y)
