import time

from Joy import *
from threading import Thread
import socket


hote = "192.168.0.36"
port = 5555

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((hote, port))

i = 0

class Controleur(Thread):

    def __init__(self, parent=None):
        super(Controleur, self).__init__(parent)
        self.bouton = 0
        self.affichage = 0
        ######
        self.i = 0
        self.liste = []
        self.etat = ""
        ######

    def notify(self, subject):
        #print("cc")
        #print(subject.get_nom())
        if subject.get_nom() == "Bouton_choix":
            self.etat = subject.get_event()
            print("choix")
            print(self.etat)
            self.update_affichage()
        if subject.get_event() == "axismotion":
            self.i = self.i + 1
            #print("entre axes")
            valeurs = subject.traitement_axes_total()
            if self.i == 1 :
                self.update_valeurs(valeurs)
                self.i = 0


        if subject.get_nom() == self.etat:
            pass
            # print("cc")


    def set_affichage(self, affichage):
        self.affichage = affichage

    def update_affichage(self):
        for l in self.liste:
            if self.etat == l.get_nom():
                print(l.get_nom())
                print(l.get_etat())
                print(l.get_message())
                print(l.get_valeurs())

    def update_valeurs(self, valeurs):
        msg = str(valeurs[0]) + ";" + str(valeurs[1])
        socket.send(msg.encode())
        print(valeurs)
        time.sleep(2)

    def add_observable(self, observable):
        print("ajout")
        self.liste.append(observable)
        #self.bouton.add_observable(observable)
        observable.add_observer(self)

    def set_choix(self, Bouton_choix):
        self.bouton = Bouton_choix

    def run(self):
        #print("run")
        while 1 :
            pass
        #print("sorti du while")



def main():
    joystick = Joy(0, 'D')
    joystick.start()
    print(joystick)

    C = Controleur()
    C.add_observable(joystick)
    C.start()
    #C.join()




if __name__ == '__main__':
    main()
