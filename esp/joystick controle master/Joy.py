import pygame
import time
from threading import Thread


class Joy(Thread):
    def __init__(self, num, side):
        Thread.__init__(self)
        pygame.init()
        pygame.joystick.init()
        self.side = side
        self.i = num
        self.joy_pygame = pygame.joystick.Joystick(self.i)
        self.joy_pygame.init()
        self.num_axe = self.joy_pygame.get_numaxes()
        self.num_buttons = self.joy_pygame.get_numbuttons()
        self.axes = []
        for i in range(self.num_axe):
            self.axes.append(0)
        self.axes_courant = self.axes.copy()
        self.buttons = []
        for i in range(self.num_buttons):
            self.buttons.append(0)
        self.buttons_courant = self.buttons.copy()
        self.courant_axe1 = 0
        self.threshold = 0.05
        self.evenement = False
        self.impression = False
        self.position = [0, 0]

        ############ variables pour le type observable
        self.observers = []
        self.event = ""
        self.nom = "Joystick"
        self.etat = "Non initialisé"
        self.valeurs = []
        self.message = "Valeurs du joystick"

    def get_message(self):
        return self.message

    def get_etat(self):
        return self.etat

    def get_event(self):
        return self.event

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_valeurs(self):
        return self.valeurs

    def get_observers(self):
        return self.observers

    def add_observer(self, obs):
        print("Ajout observateur")
        if not hasattr(obs, 'notify'):
            raise ValueError("First argument must be object with notify method")
        self.observers.append(obs)
        self.notify_observer("add user")

    def delete_observer(self, obs):
        if obs in self.observers:
            self.observers.remove(obs)

    def is_registered_observer(self, obs):
        if obs in self.observers:
            return True
        else:
            return False

    def is_observable(self):
        return True

    def notify_observer(self, event):
        self.event = event
        for obs in self.observers:
            thread = Thread(target=obs.notify, args=(self,))
            thread.start()
            # obs.notify(self)

    def traitement_axes_total(self):
        self.impression = False
        Tampon = self.axes_courant.copy()
        for i in range(self.num_axe):
            self.axes_courant[i] = self.joy_pygame.get_axis(i)
            if abs(self.axes_courant[i] - Tampon[i]) > self.threshold:
                self.impression = True
                rang = i
        if self.impression:
            # print ("class joy : ")
            # print(self.axes_courant)
            # print(rang)
            self.position = [round(self.axes_courant[1] * -100),round(self.axes_courant[0] * 100), ]
        return self.position

    def traitement_axes_wot(self):
        impression = False
        self.evenement = False
        self.courant_axe1 = self.joy_pygame.get_axis(1)
        if abs(self.courant_axe1) > self.threshold:
            self.evenement = True
            impression = True
        if impression:
            if self.courant_axe1 > 0:

                self.courant_axe1 = -1
                # print(self.courant_axe1)
                time.sleep(0.1)
            else:
                self.courant_axe1 = 1
                # print(self.courant_axe1)
                time.sleep(0.1)

    def verif_axe1(self):
        if abs(self.joy_pygame.get_axis(1)) > self.threshold:
            if self.joy_pygame.get_axis(1) > 0:
                return -1
            else:
                return 1
        else:
            return 0

    def appuie_bouton(self):
        impression = False
        # print('boutton pressé ')
        Tampon = self.buttons_courant.copy()
        for i in range(self.num_buttons):
            self.buttons_courant[i] = self.joy_pygame.get_button(i)
            if self.buttons_courant[i] != Tampon[i]:
                impression = True
                if impression:
                    try:
                        # print(self.buttons_courant,self.buttons_courant.index(1)+1)
                        return self.buttons_courant.index(1) + 1
                    except:
                        pass

    def run(self):
        print('CC')
        self.etat = "thread lancé"
        print(pygame.get_init())
        while 1:
            for event in pygame.event.get():
                #print('event')
                #print(event.type)
                if event.type == pygame.JOYBUTTONDOWN:
                    #print(self.side + 'boutton presse ')
                    self.notify_observer("buttondown")

                if event.type == pygame.JOYBUTTONUP:
                    #print(self.side + 'boutton relaché ')
                    self.notify_observer("buttonup")

                if event.type == pygame.JOYHATMOTION:
                    #print(self.side + 'hat pressé ')
                    self.notify_observer("hatmotion")
                if event.type == pygame.JOYAXISMOTION:
                    #print("axis joy")
                    self.notify_observer("axismotion")


def main():
    joystick = Joy(0, 'D')
    joystick.start()
    joystick.join()



if __name__ == '__main__':
    main()
