from machine import Pin,PWM

enA = PWM(Pin(5))
in1 = Pin(4,Pin.OUT)
in2 = Pin(0,Pin.OUT)

enB = PWM(Pin(14))
in4 = Pin(12,Pin.OUT)
in5 = Pin(13,Pin.OUT)

enA.freq(500)
enB.freq(500)

#enA.duty(250)
#enB.duty(250)

b = 0.125
r = 0.035

def arret_complet():
    in1.off()
    in2.off()
    in4.off()
    in5.off()

def ordre_recu(V,W):
    wr = (b/(r**2))*((r/b)*V+(r/2)*W)
    wl = (b/(r**2))*((r/b)*V-(r/2)*W)
    print("wr = ",wr,'wl = ',wl)
    return wr,wl

def vitesse_moteur_droite(val):
    print("val moteur droite : ",val)
    if val > 0 :
        in1.on()
        in2.off()
        if val > 1024 :
            enA.duty(1024)
        else:
            enA.duty(int(val))
    else:
        in1.off()
        in2.on()
        if abs(val) > 1024 :
            enA.duty(1024)
        else:
            enA.duty(int(abs(val)))
    
def vitesse_moteur_gauche(val):
    print("val moteur gauche : ",val)
    if val > 0 :
        in4.on()
        in5.off()
        if val > 1024 :
            enB.duty(1024)
        else:
            enB.duty(int(val))
    else:
        in4.off()
        in5.on()
        if abs(val) > 1024 :
            enB.duty(1024)
        else:
            enB.duty(int(abs(val)))
    
    
def commande(U):
    wr,wl = ordre_recu(U[0],U[1])
    if wr == 0  and wl == 0:
        arret_complet()
    else:
        vitesse_moteur_droite(wr)
        vitesse_moteur_gauche(wl)


def sens_moteur1():
    """avancer"""
    in1.on()
    in2.off()
    in4.on()
    in5.off()
    
def sens_moteur2():
    """reculer """
    in1.off()
    in2.on()
    in4.off()
    in5.on()
    
def vitesse(val):
    enA.duty(val)
    enB.duty(val)
    
    
    
import socket

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind(('',5555))
socket.listen(1)
client,adresse = socket.accept()
print(adresse)
print(client)
while True :
    
    recu = client.recv(255)
    recu = recu.decode()
    if recu !="":
        print(recu)
        #R = recu.decode()
        L = recu.split(";")
        print(L)
        l = [float(l) for l in L]
        print(l)
        commande([float(l) for l in L])
        
client.close()
socket.close()

        



