import paho.mqtt.client as mqtt #import the client1
import time
############
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    #print("message topic=",message.topic)
    #print("message qos=",message.qos)
    #print("message retain flag=",message.retain)
########################################
broker_address="127.0.0.1"
print("creating new instance")
client = mqtt.Client("Pc_fixe") #create new instance
client.on_message=on_message #attach function to callback
print("connecting to broker")
client.connect(broker_address,port=2000) #connect to broker
client.loop_start() #start the loop
print("Subscribing to topic","agent1/vitesse")
client.subscribe("Agent1retour")
print("Publishing message to topic","agent1/vitesse")
client.publish("Agent1","0.4")
time.sleep(4) # wait

while 1 :
    i = input("Valeur a envoyer : ")
    client.publish("Agent1",i)

client.loop_stop() #stop the loop