from __future__ import division


def integration(G, q0, T_final, qp0=0, Q_des=0, affichage=False, Teval=0):
    import time
    import networkx as nx
    import numpy as np
    import pylab as plt
    from scipy.integrate import solve_ivp
    from sympy import init_printing, Matrix, ones, lambdify, MatrixSymbol, BlockMatrix
    init_printing()
    L = nx.laplacian_matrix(G)
    L = L.toarray()
    Agents = [n for n in G.nodes()]
    deg = L[0, 0]

    Nb_param = 2  # p = [m g]
    Nb_agents = len(Agents)

    # print("Matrice laplacienne : ")
    # print(L)
    # print("Nb_param : ", Nb_param)
    # print("Nb_agents : ", Nb_agents)

    ############ Définition du symbolique ############
    # print("----------------- Défintion Symbolique --------------------------")
    ### vecteur parametres reel ###
    p = Matrix(MatrixSymbol('p', Nb_param, 1))
    ### vecteur parametres controleur ###
    pc = Matrix(MatrixSymbol('pc', Nb_param, 1))
    ### vecteur des coef de réglages ###
    kc = Matrix(MatrixSymbol('kc', 2, 1))
    ### definition du vecteur etat ####
    Z = Matrix(MatrixSymbol('Z', Nb_agents, 1))
    Zp = Matrix(MatrixSymbol('Zp', Nb_agents, 1))
    Q = Matrix(BlockMatrix([[Z], [Zp]]))
    Z_des = Matrix(MatrixSymbol('Z_des', Nb_agents, 1))
    ### definition du vecteur d'entrées ###
    U = Matrix(MatrixSymbol('U', Nb_agents, 1))
    ### definition de la matrice de sensibilité ###
    Pi = Matrix(MatrixSymbol('Pi', Nb_agents * 2, Nb_param))
    ### defintion de h ####
    h = - kc[0] * L * (Z - Z_des) - kc[1] * L * Zp + pc[0] * pc[1] * ones(Nb_agents, 1)
    ### definition de f ####
    f = Matrix(BlockMatrix([[Zp], [-p[1] * ones(Nb_agents, 1) + U / p[0]]]))
    ### calcul des jacobiennes ####
    df_dq = f.jacobian(Q)
    df_dp = f.jacobian(p)
    df_du = f.jacobian(U)

    dh_dzdes = h.jacobian(Z_des)
    dZes_dp = Z_des.jacobian(p)
    dh_dq = h.jacobian(Q)
    dh_dpc = h.jacobian(pc)
    dpc_dp = pc.jacobian(p)
    dh_dkc = h.jacobian(kc)
    dkc_dp = kc.jacobian(p)

    ### définition de la sensibilité des entrées ####
    Theta = dh_dzdes * dZes_dp + dh_dq * Pi + dh_dpc * dpc_dp + dh_dkc * dkc_dp

    Pip = df_dq * Pi + df_du * Theta + df_dp
    # print("----------------------Fin symbolique--------------------")

    pip_numerique = lambdify([Q, Z_des, p, pc, kc, Pi, U], Pip, 'numpy')
    Qp = lambdify([Q, U, p], f, 'numpy')
    entree = lambdify([Q, Z_des, pc, kc], h, "numpy")

    def dynamique_entre(t, Q):
        return Qp(Q, entree(Q, Q_des, pc, kc), p)

    #### fonction a integrer avec un vecteur d'état étendu pour calcul Pi aussi ###
    def intregante(t, Q_bar):
        Q = Q_bar[:Nb_agents * 2]
        Pi = Q_bar[Nb_agents * 2:]
        # print(Pi)
        P1 = Pi
        Pi = Pi.reshape((2 * Nb_agents, Nb_param))
        # print(Pi)
        Qr = dynamique_entre(t, Q)
        Qr = Qr.reshape((1, Nb_agents * 2))
        # print(Q2)
        Pi_r = pip_numerique(Q, Z_des, p, pc, kc, P1, entree(Q, Q_des, pc, kc))
        Pi_r = Pi_r.reshape((1, 2 * Nb_agents * Nb_param))
        Q_bar_r = np.hstack((Qr[0], Pi_r[0]))
        # print(Q_bar_r)
        # print(t)
        return Q_bar_r

    # print("------------ Definir les valeurs initiale -------------------")

    # q0 = np.array([0, 2, 4, 6, 8])  # ,2,-1,8,-5])
    if qp0 == 0:
        qp0 = np.array([0] * Nb_agents)
    if Q_des == 0:
        Q_des = np.array([0] * Nb_agents)

    pc = np.array([2, 9.81])
    p = np.array([2, 9.81])
    kc = np.array([5, 10])

    ### mise en forme des données ####
    """ forme du vecteur d'etat [z0,...,zi,...,zn,z0p,...,zip,...,znp] avec zi l'altitude de l'agent i et zip sa vitesse """
    Pi0 = np.zeros((1, 2 * Nb_agents * Nb_param))
    Q = np.concatenate((q0, qp0))  # mise en forme du vecteur etat
    Q_bar = np.hstack((Q, Pi0[0]))  # mise en forme des parametres initiaux vecteur etat etendu

    ### definition des parametres d'intégrations ####
    # T = np.linspace(0, T_final, 10000)
    ### integration ###
    start_time = time.time()
    if Teval.all() :
        Y = solve_ivp(intregante, [0, T_final], Q_bar)  # ,t_eval=T)
    else:
        Y = solve_ivp(intregante, [0, T_final], Q_bar, t_eval=Teval)  # ,t_eval=T)

    t = (time.time() - start_time)
    # print("temps d'éxecution : %s " % t)

    if affichage:
        nx.draw(G, with_labels=True)  # affichage graph
        for i in range(0, Nb_agents):
            plt.figure(3)
            plt.plot(Y.t, Y.y[i], label="q" + str(i + 1))
            plt.legend()
            plt.figure(4)
            plt.plot(Y.t, Y.y[Nb_agents + i], label="qp" + str(i + 1))
            plt.legend()
        plt.show()

        for k in range(2 * Nb_agents, 3 * Nb_agents):
            for j in range(Nb_param):
                plt.figure(5 + j)
                plt.plot(Y.t, Y.y[k + j], label="dq" + str(k + 1 - (2 * Nb_agents)) + "dp" + str(j))
                plt.legend()
        plt.show()
        for k in range(3 * Nb_agents, 4 * Nb_agents):
            for j in range(Nb_param):
                plt.figure(5 + Nb_param + 1 + j)
                plt.plot(Y.t, Y.y[k + j], label="dqp" + str(k + 1 - (3 * Nb_agents)) + "dp" + str(j))
                plt.legend()
        plt.show()

    return [deg, Nb_agents, G, Y, t]  # , Pi_min, Pi_max, Pi_mean, Pi_ptp, Pi_std, Pi_med]
