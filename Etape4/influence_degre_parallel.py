import time
import numpy as np
import networkx as nx
from sol_consensus import integration
import pylab as plt
from joblib import Parallel,delayed
import multiprocessing

Nb_agents = 50
L_degre = [2 * k for k in range(1, (Nb_agents // 2) + 1)]
print(L_degre)
q0 = [2 * k for k in range(Nb_agents)]
print(q0)
T_final = 20
T = np.linspace(0,T_final,1000)
S = []
def calcul(deg):
    G = nx.circulant_graph(Nb_agents, [k for k in range((deg // 2) + 1)])
    #print("----------------------Début calcul degre : ",deg, "------------------")
    S_new = integration(G, q0, T_final,Teval = T )
    #print("degré : ", deg)
    #print("résulat : ", S_new[4:])
    #S.append(S_new)
    return S_new
    #print("----------------------Fin calcul------------------")


num_cores = multiprocessing.cpu_count()
print(num_cores)
start_time = time.time()
S = Parallel(n_jobs=num_cores)(delayed(calcul)(deg) for deg in L_degre)
t = (time.time() - start_time)
print("temps d'éxecution : %s " % t)



def extraction_matrice_norme():
    for s in S:
        sol=s[3]
        pic = sol.y[Nb_agents * 2:]
        (a, b) = pic.shape
        nb_param = int(a / (Nb_agents * 2))
        pic = pic.reshape((Nb_agents * 2, nb_param, b))
        Mt = []
        Mt_normaliser = []
        Norme = []
        Norme_normaliser = []
        for k in range(b):
            M = np.zeros((Nb_agents * 2, nb_param))
            for i in range(Nb_agents * 2):
                for j in range(nb_param):
                    M[i, j] = pic[i, j][k]
            if M.min()+M.max()==0:
                M2 = np.zeros((Nb_agents * 2, nb_param))
            else:
                #print(M.min(),M.max())
                #print(M.all())
                M2 = (M - M.min()) / (M.max() - M.min())
            Mt_normaliser.append(M2)
            Norme_normaliser.append(np.linalg.norm(M2))
            Mt.append(M)
            Norme.append(np.linalg.norm(M))
        s.append([Mt,Norme,Mt_normaliser,Norme_normaliser])

extraction_matrice_norme()

def affichage():
    for i in range(len(S)):
        plt.figure(1)
        plt.plot(T,S[i][-1][1],label="deg : "+str(S[i][0]))
        plt.legend()
        plt.figure(2)
        plt.plot(T,S[i][-1][-1],label="deg : "+str(S[i][0]))
        plt.legend()
    plt.show()
affichage()
"""
Pi_stat= [[[],"t_exe"],[[],"Pi_min"],[[],"Pi_max"],[[],"Pi_mean"],[[],"Pi_ptp"],[[],"Pi_std"],[[],"Pi_med"]]

for k in range(len(L_degre)):
    for j in range(len(Pi_stat)):
        Pi_stat[j][0].append(S[k][4+j])

def affichage():
    for k in range(len(Pi_stat)):
        plt.figure(k)
        plt.plot(L_degre, Pi_stat[k][0], '>', label=Pi_stat[k][1])
        plt.legend()
        plt.show()

affichage()


Pi_r = Y.y[Nb_agents * 2:]
Pi_min = np.amin(Pi_r)
Pi_max = np.amax(Pi_r)
Pi_mean = np.mean(Pi_r)
Pi_ptp = np.ptp(Pi_r)
Pi_std = np.std(Pi_r)
Pi_med = np.median(Pi_r)

"""