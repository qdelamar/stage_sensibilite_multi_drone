import networkx as nx
import pylab as plt
#G = nx.binomial_graph(3,10) ## degre n-1 si p !=0 p = proba de creation
#G = nx.complete_graph(60) # degre n-1
#G = nx.complete_multipartite_graph(20,20,10) # pas compris
#G = nx.circular_ladder_graph(5)  #degre 3 et 2n agents
deg = 5
G = nx.circulant_graph(40,[k for k in range((deg//2)+1)]) # n agents et L[-1]*2 degres degres max n-1
#G = nx.cycle_graph(10) # degre 2
#G = nx.dorogovtsev_goltsev_mendes_graph(3)
#G = nx.full_rary_tree(20,40)
#G = nx.turan_graph(10,5)

L = nx.laplacian_matrix(G)
L = L.toarray()
Agents = [n for n in G.nodes()]
print(L)
nx.draw(G, with_labels=True)  # affichage graph
plt.show()