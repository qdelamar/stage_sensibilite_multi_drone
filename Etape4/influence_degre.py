import time

import networkx as nx
from sol_consensus import integration
import pylab as plt
from joblib import Parallel,delayed
import multiprocessing

Nb_agents = 20
L_degre = [2 * k for k in range(1, (Nb_agents // 2) + 1)]
print(L_degre)
q0 = [2 * k for k in range(Nb_agents)]
print(q0)
T_final = 20
S = []
def calcul():
    for deg in L_degre:
        G = nx.circulant_graph(Nb_agents, [k for k in range((deg // 2) + 1)])
        # print("----------------------Début calcul degre : ",deg, "------------------")
        S_new = integration(G, q0, T_final)
        # print("degré : ", deg)
        # print("résulat : ", S_new[4:])
        S.append(S_new)
    return S





start_time = time.time()
S = calcul()
t = (time.time() - start_time)
print("temps d'éxecution : %s " % t)
Pi_stat= [[[],"t_exe"],[[],"Pi_min"],[[],"Pi_max"],[[],"Pi_mean"],[[],"Pi_ptp"],[[],"Pi_std"],[[],"Pi_med"]]

for k in range(len(L_degre)):
    for j in range(len(Pi_stat)):
        Pi_stat[j][0].append(S[k][4+j])

def affichage():
    for k in range(len(Pi_stat)):
        plt.figure(k)
        plt.plot(L_degre, Pi_stat[k][0], '>', label=Pi_stat[k][1])
        plt.legend()
        plt.show()

affichage()