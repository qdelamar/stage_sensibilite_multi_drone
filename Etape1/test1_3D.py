import time

import matplotlib.pyplot as plt
import networkx as nx
from mpl_toolkits.mplot3d import Axes3D
from numpy import array, dot, ravel, mean
# import pylab as plt
from ode_helpers import state_plotter
from scipy.integrate import solve_ivp

##Création du graphe
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])

L = nx.laplacian_matrix(G)  # matrice creuse

L = L.toarray()  # matrice complete pour le calcul et l'affichage
L2 = array([L, L, L])
# print(L2)
X2 = array([[0, 1, 2], [3, 4, 6], [8, 10, 14], [-6, -5, -5], [-4, -5, -7]])
B = -dot(L2, X2)
print(B)
print(B[0])

A2 = X2.reshape((1, 15))
print(L)
nx.draw(G, with_labels=True)
A2 = A2.tolist()

X_desire = array([[]])


# fct a intégrer
def fct(t, x):
    # print(x)
    y = array(x)
    # print(y)
    y = y.reshape((5, 3))
    # print(y)
    # print(y)
    # L = array([[1,-1,0,0,0],[-1,3,-1,0,-1],[0,-1,3,-1,-1],[0,0,-1,2,-1],[0,-1,-1,-1,3]])
    r = -dot(L2, y)
    # print(r)
    r = r[0]
    r = r.reshape((1, 15))
    return r


# Etat initial
X0 = [0, 2, 4, -6, -4]

## intégration avec des parametre reglable pour l'instant en mode auto
start_time = time.time()
Y = solve_ivp(fct, [0, 15], A2[0])
print("temps d'éxecution : %s " % (time.time() - start_time))
plt.figure(2)
plt.plot(Y.t, Y.y[0])
plt.plot(Y.t, Y.y[1])
plt.plot(Y.t, Y.y[2])
plt.plot(Y.t, Y.y[3])
plt.plot(Y.t, Y.y[4])

state_plotter(Y.t, Y.y, 3)

fig = plt.figure(5)
ax = Axes3D(fig)
ax.plot3D(ravel(Y.y[0]), ravel(Y.y[1]), ravel(Y.y[2]))
ax.plot3D(ravel(Y.y[3]), ravel(Y.y[4]), ravel(Y.y[5]))
ax.plot3D(ravel(Y.y[6]), ravel(Y.y[7]), ravel(Y.y[8]))
ax.plot3D(ravel(Y.y[9]), ravel(Y.y[10]), ravel(Y.y[11]))
ax.plot3D(ravel(Y.y[12]), ravel(Y.y[13]), ravel(Y.y[14]))
fig.add_axes(ax)
# plt.show()

A1_final = [Y.y[0][-1], Y.y[1][-1], Y.y[2][-1]]
A2_final = [Y.y[3][-1], Y.y[4][-1], Y.y[5][-1]]
A3_final = [Y.y[6][-1], Y.y[7][-1], Y.y[8][-1]]
A4_final = [Y.y[9][-1], Y.y[10][-1], Y.y[11][-1]]
A5_final = [Y.y[12][-1], Y.y[13][-1], Y.y[14][-1]]
A1_init = X2[0]
A2_init = X2[1]
A3_init = X2[2]
A4_init = X2[3]
A5_init = X2[4]
# todo Ameliorer affichage + regarder axis=0 et stockage ous forme matriciel
A_moyen = mean(X2, axis=0)
Erreur_1 = abs(A_moyen - A1_final)
Erreur_2 = abs(A_moyen - A2_final)
Erreur_3 = abs(A_moyen - A3_final)
Erreur_4 = abs(A_moyen - A4_final)
Erreur_5 = abs(A_moyen - A5_final)
