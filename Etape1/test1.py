import time

import networkx as nx
import pylab as plt
from numpy import array, dot
from ode_helpers import state_plotter
from scipy.integrate import solve_ivp

##Création du graphe
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])

L = nx.laplacian_matrix(G)  # matrice creuse
# print(L)

L = L.toarray()  # matrice complete pour le calcul et l'affichage

print(L)
nx.draw(G, with_labels=True)

X_desire = [-8, -6, -4, -2, 0]


# fct a intégrer
#
#
def fct(t, x):
    # L = array([[1,-1,0,0,0],[-1,3,-1,0,-1],[0,-1,3,-1,-1],[0,0,-1,2,-1],[0,-1,-1,-1,3]])
    r = (-dot(L, x - X_desire))
    # print(r)
    return r


# Etat initial
X0 = array([0, 2, 4, -6, -4])

## intégration avec des parametre reglable pour l'instant en mode auto
start_time = time.time()
Y = solve_ivp(fct, [0, 5], X0)
print("temps d'éxecution : %s " % (time.time() - start_time))
plt.figure(2)
plt.plot(Y.t, Y.y[0])
plt.plot(Y.t, Y.y[1])
plt.plot(Y.t, Y.y[2])
plt.plot(Y.t, Y.y[3])
plt.plot(Y.t, Y.y[4])

state_plotter(Y.t, Y.y, 3)

plt.show()
