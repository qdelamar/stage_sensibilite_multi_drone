import networkx as nx
import pylab as plt
from numpy import *
from ode_helpers import state_plotter
from scipy.integrate import solve_ivp

""" 
équation  choisit ypp = -g -k/m *L *Y
avec k un coefficient de reglage regle a 1 pour l'instant 
m la masse du drone et L la matrice laplacienne du graphe donc controle en force proportionnelle  à la distance avec les autres drones afin de les mettres
tous sur le même plan.
"""

##Création du graphe
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])

L = nx.laplacian_matrix(G)  # matrice creuse
# print(L)

L = L.toarray()  # matrice complete pour le calcul et l'affichage

# print(L)
# nx.draw(G, with_labels=True)

### parametre
g = 9.81
m = 2
k = 2
Z0 = array([0, 2, 4, 6, 8])
Z_mean = mean(Z0)
print("Altitude attendu : ", Z_mean)
""" forme du vecteur d'etat [z0,...,zi,...,zn,z0p,...,zip,...,znp] avec zi l'altitude de l'agent i et zip sa vitesse """
Z0P = array([0, 0, 0, 0, 0])

Z = concatenate((Z0, Z0P))


### Dynamique

def Dynamique(t, Z):
    # ZPP = -g+ g -dot(block_diag(L,L), Z)*(k/m)
    # ZPP = ones((1,5))*sin(t)
    ZPP = -g * ones((1, 5)) + (U(t, Z) / m)
    # print(ZPP[0])
    ZP = Z[5:]
    res = concatenate((ZP, ZPP[0]))
    # print(res)
    return res


### fonction d'entrée

def U(t, Z):
    # print("entre : ",Z)
    res = -(k) * dot(L, Z[:5]) - (k) * dot(L, Z[5:])  # +m*g * ones((1,5))
    # print(res)
    return res[0]


##

T = linspace(0, 10, 100000)
Y = solve_ivp(Dynamique, [0, 30], Z, t_eval=T)

plt.figure(2)
plt.plot(Y.t, Y.y[0])
plt.plot(Y.t, Y.y[1])
plt.plot(Y.t, Y.y[2])
plt.plot(Y.t, Y.y[3])
plt.plot(Y.t, Y.y[4])
plt.figure(4)
plt.plot(Y.t, Y.y[5])
plt.plot(Y.t, Y.y[6])
plt.plot(Y.t, Y.y[7])
plt.plot(Y.t, Y.y[8])
plt.plot(Y.t, Y.y[9])

state_plotter(Y.t, Y.y, 3)

# plt.show()
print(Dynamique(0, Z))
