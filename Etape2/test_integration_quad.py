import networkx as nx
import pylab as plt
from numpy import *
from ode_helpers import state_plotter
from scipy.integrate import solve_ivp

##Création du graphe
G = nx.Graph()

G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (2, 3), (2, 5), (3, 5), (3, 4), (5, 4)])

L = nx.laplacian_matrix(G)  # matrice creuse
# print(L)

L = L.toarray()  # matrice complete pour le calcul et l'affichage

print(L)
nx.draw(G, with_labels=True)
Y0 = array([[0, 0], [2, 0], [4, 0], [-6, 0], [-4, 0]])
Y0_l = Y0.reshape((1, Y0.size))
Y0_liste = Y0_l.tolist()

g = 9.81
f = 10
m = 1


def dynamique(t, Y):
    Y_v = Y.reshape((5, 2))
    # print(Y_v)
    Y_new = -dot(array([L, L]), Y_v)
    # print(Y_new)
    (i_max, j_max) = Y_new[0].shape
    Yp = zeros((5, 2))
    # print(Yp)
    # print(i_max)

    for i in range(i_max):
        Yp[i, 0] = Y_new[0, i, 1]
        Yp[i, 1] = -g + f / m
    # print(Yp)
    Yp_l = Yp.reshape((1, Yp.size))

    return Yp_l


Y = solve_ivp(dynamique, [0, 10], Y0_liste[0])

plt.figure(2)
plt.plot(Y.t, Y.y[0])
plt.plot(Y.t, Y.y[1])
plt.plot(Y.t, Y.y[2])
plt.plot(Y.t, Y.y[3])
plt.plot(Y.t, Y.y[4])
plt.plot(Y.t, Y.y[5])
plt.plot(Y.t, Y.y[6])
plt.plot(Y.t, Y.y[7])
plt.plot(Y.t, Y.y[8])
plt.plot(Y.t, Y.y[9])

state_plotter(Y.t, Y.y, 3)

plt.show()
